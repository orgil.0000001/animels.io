using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public enum AnimTp { Idle, Run, Win, Death }
public enum TailTp { Body, Tail, House }
public class Tail {
    public static Vector2Int animal;
    public static Vector3 Limit;
    public static Dictionary<AnimTp, Tail> AnimDic = new Dictionary<AnimTp, Tail>();
    public Character par = null;
    public TailTp tp;
    public bool isHouse => tp == TailTp.House;
    public bool isVisible = true;
    public Transform tf;
    public GameObject go;
    public Vector3 Tp => tf.position;
    public SkinnedMeshRenderer smr;
    float curAng, ang, t = 0, dt = 0, dis;
    Character tmpChar;
    AnimTp anim = AnimTp.Idle;
    public Tail(GameObject go, TailTp tp) {
        this.go = go;
        this.tp = tp;
        tf = go.transform;
        smr = ShowAnimal(go);
        if (tp != TailTp.Body) AnimalMat(null);
    }
    public void Anim(AnimTp tp) { anim = tp; }
    public void Animate() {
        if (isVisible) {
            Tail a = AnimDic[anim];
            int i, cnt;
            for (i = 0, cnt = smr.bones.Length; i < cnt; i++) {
                smr.bones[i].localPosition = a.smr.bones[i].localPosition;
                smr.bones[i].localRotation = a.smr.bones[i].localRotation;
                smr.bones[i].localScale = a.smr.bones[i].localScale;
            }
            // for (i = 0, cnt = smr.sharedMesh.blendShapeCount; i < cnt; i++)
            //     smr.SetBlendShapeWeight(i, a.smr.GetBlendShapeWeight(i));
        }
    }
    public void Update() {
        isVisible = HWP.IsOnScreenBox(Tp, 2, 2);
        if (isVisible != smr.enabled) smr.enabled = isVisible;
        if (!par && tp == TailTp.Tail) {
            dt += Mb.Dt;
            if (dt > t) {
                dt = 0;
                t = Rnd.Rng(4f, 12f);
                ang = Rnd.Ang;
            }
            curAng = M.Lerp(curAng, ang, Mb.Dt * .5f);
            tf.rotation = Q.Y(curAng);
            tf.position = V3.C(Tp + tf.forward * .2f * Mb.Dt, -Limit, Limit);
        }
    }
    public void Upd(Character par2, bool isAnim = true) {
        tmpChar = par;
        par = par2;
        if (tmpChar) tmpChar.RmvTails();
        AnimalMat(par);
        if (isAnim) Anim(par ? AnimTp.Run : AnimTp.Idle);
    }
    public void Move((Vector3, Quaternion) pnt) {
        dis = V3.Dis(Tp, pnt.Item1);
        if (dis <= 0.1f) {
            tf.position = pnt.Item1;
        } else {
            tf.position = V3.Mv(Tp, pnt.Item1, Mb.FixDt * par.spd * (1 + (M.Ceil(dis / 1) + 1) * .4f));
        }
        if (Q.Ang(tf.rotation, pnt.Item2) <= 5) {
            tf.rotation = pnt.Item2;
        } else {
            tf.rotation = Q.Lerp(tf.rotation, dis <= 2 ? pnt.Item2 : Q.Y(Ang.Xz(Tp, pnt.Item1)), Mb.FixDt * 10);
        }
    }
    void AnimalMat(Character par) {
        go.Child(0, animal.x, animal.y).RenMat(par ? par.Child(0, animal.x, animal.y).RenMat() : Gc._.tailMat);
    }
    public static SkinnedMeshRenderer ShowAnimal(GameObject go) {
        var smr = go.Child<SkinnedMeshRenderer>(0, animal.x, animal.y);
        smr.Show();
        return smr;
    }
}