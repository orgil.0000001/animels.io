﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Cm : Singleton<Cm> { // Camera Controller
    public Vector2 offset = V2.V(10, 30);
    Vector3 pOff;
    float off;
    void Start() {
        pOff = (Tp - P.Tp).normalized;
        off = offset.x;
    }
    void Update() {
        if (IsGame) {
            off = M.Lerp(off, M.Remap(P.tailCount, P.startAnimal, 100, offset.x, offset.y), Dt * 10);
            Tp = P.Tp + pOff * off;
        }
    }
}
// void GoingBalls(float scapeAng = 30, float scapeSpd = 10) {
//     float ang = Ang.DAng(P.Tle.y, Tle.y);
//     if (ang > 180 - scapeAng) {
//         Tp += go.TfDir(V3.Xz(1, 0.5f)) * scapeSpd * Dt;
//     } else if (ang < scapeAng - 180) {
//         Tp += go.TfDir(V3.Xz(-1, 0.5f)) * scapeSpd * Dt;
//     }
//     Tp = V3.MvUnc(P.Tp.Y(0), Tp.Y(0), V3.Dis(V3.O, pOff.Y(0))).Y(P.Tp.y + pOff.y);
//     Te = V3.Xy(Te.x, Ang.Xz(Tp, P.Tp));
// }