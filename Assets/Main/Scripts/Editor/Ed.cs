﻿using UnityEditor;
using UnityEngine;
using System;
using Orgil;

[CustomPropertyDrawer(typeof(DrawIfAttribute))]
public class DrawIfPropertyDrawer : PropertyDrawer {
    DrawIfAttribute drawIf;
    SerializedProperty comparedField;
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
        if (!ShowMe(property) && drawIf.disablingType == DrawIfAttribute.DisablingType.DontDraw)
            return 0f;
        return base.GetPropertyHeight(property, label);
    }
    private bool ShowMe(SerializedProperty property) {
        drawIf = attribute as DrawIfAttribute;
        string path = property.propertyPath.Contains(".") ? System.IO.Path.ChangeExtension(property.propertyPath, drawIf.comparedPropertyName) : drawIf.comparedPropertyName;
        comparedField = property.serializedObject.FindProperty(path);
        if (comparedField == null) {
            Debug.LogError("Cannot find property with name: " + path);
            return true;
        }
        switch (comparedField.type) {
            case "bool": return comparedField.boolValue.Equals(drawIf.comparedValue);
            case "int": return comparedField.intValue.Equals(drawIf.comparedValue);
            case "Enum": return comparedField.enumValueIndex.Equals((int)drawIf.comparedValue);
            default: Debug.LogError("Error: " + comparedField.type + " is not supported of " + path); return true;
        }
    }
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        if (ShowMe(property)) {
            EditorGUI.PropertyField(position, property);
        } else if (drawIf.disablingType == DrawIfAttribute.DisablingType.ReadOnly) {
            GUI.enabled = false;
            EditorGUI.PropertyField(position, property);
            GUI.enabled = true;
        }
    }
}
public static class Ed { // Editor Utils
    [MenuItem("Utils/Simple Settings")]
    public static void SimpleSettings() {
        PlayerSettings.companyName = "Miga";
        PlayerSettings.productName = Cc._.menuGo.ChildName("Title").Txt().text.Pc(" ");
        PlayerSettings.defaultInterfaceOrientation = UIOrientation.Portrait;
        PlayerSettings.applicationIdentifier = "com.mireedui." + PlayerSettings.productName.Pc();
    }
    [MenuItem("Utils/Android Settings")]
    public static void AndroidSettings() {
        SimpleSettings();
        PlayerSettings.Android.minSdkVersion = AndroidSdkVersions.AndroidApiLevel22;
        PlayerSettings.SetScriptingBackend(BuildTargetGroup.Android, ScriptingImplementation.IL2CPP);
        PlayerSettings.Android.targetArchitectures = AndroidArchitecture.ARMv7 | AndroidArchitecture.ARM64;
    }
    [MenuItem("Utils/Clear Data")]
    public static void ClearData() { PlayerPrefs.DeleteAll(); }
    [MenuItem("Utils/Screenshot _F10")]
    public static void Screenshot() {
        O.Screenshot("Screenshots/", "Screenshot" + System.DateTime.Now.ToString("_yyyy-MM-dd_hh-mm-ss"));
        Debug.Log("Screenshot" + System.DateTime.Now.ToString("_yyyy-MM-dd_hh-mm-ss"));
    }
    [MenuItem("Utils/Pause _F11")]
    public static void Pause() { EditorApplication.isPaused = !EditorApplication.isPaused; }
    [MenuItem("Utils/Step _F12")]
    public static void Step() { EditorApplication.Step(); }
    [MenuItem("Utils/Replay &UP")]
    public static void Replay() {
        if (Application.isPlaying)
            Cc._.Replay();
    }
    [MenuItem("Utils/Previous Level &LEFT")]
    public static void PrvLvl() {
        if (Application.isPlaying && Gc.Level > 1) {
            Gc.Level--;
            Cc._.Replay();
        }
    }
    [MenuItem("Utils/Next Level &RIGHT")]
    public static void NxtLvl() {
        if (Application.isPlaying) {
            Gc.Level++;
            Cc._.Replay();
        }
    }
    public static T LoadAsset<T>(string path) { return (T)(object)AssetDatabase.LoadAssetAtPath(path, typeof(T)); }
    public static void SaveOpenScenes() { UnityEditor.SceneManagement.EditorSceneManager.SaveOpenScenes(); }
}
//public const string Ctrl = "%", Shift = "#", Alt = "&",
//    Left = "LEFT", Right = "RIGHT", Up = "UP", Down = "DOWN",
//    F1 = "F1", F2 = "F2", F3 = "F3", F4 = "F4", F5 = "F5", F6 = "F6", F7 = "F7", F8 = "F8", F9 = "F9", F10 = "F10", F11 = "F11", F12 = "F12",
//    Home = "HOME", End = "END", PgUp = "PGUP", PgDn = "PGDN",
//    CtrlShiftAlt = Ctrl + Shift + Alt, CtrlShift = Ctrl + Shift, CtrlAlt = Ctrl + Alt, ShiftAlt = Shift + Alt;
//static float dAng = 10;
//static void Active(GameObject go) { Selection.activeGameObject = go; }
//static void Focus() { SceneView.FrameLastActiveSceneView(); }
//static void ActiveFocus(GameObject go) {
//    Active(go);
//    Focus();
//}
//// l r f b t b => y90 y-90 y180 y0 x90 x-90
//static void SceneViewNavigation(float x, float y, float z) { SceneView.lastActiveSceneView.rotation = Q.E(x, y, z); }
//static void SceneViewNavigation(Vector3 rot) { SceneView.lastActiveSceneView.rotation = Q.E(rot); }
//// l r u d => y -y x -x
//static void SceneViewNavigationRotate(float x, float y, float z) { SceneViewNavigation(SceneView.lastActiveSceneView.rotation.eulerAngles + V3.V(x, y, z)); }
//public static void PlayerSettingsUpd() {
//    PlayerSettings.SplashScreen.show = false;
//    PlayerSettings.SplashScreen.logos = null;
//    Texture2D icon = LoadAsset<Texture2D>("Assets/Main/Sprites/icon.png");
//    if (icon)
//        PlayerSettings.SetIconsForTargetGroup(BuildTargetGroup.Unknown, new Texture2D[] { icon });
//    PlayerSettings.SplashScreen.show = true;
//    if (iphone)
//        PlayerSettings.SplashScreen.background = TexSpr(iphone);
//    Texture2D iphone = GC.iPhoneLaunchScreen, ipad = GC.iPadLaunchScreen;
//    PlayerSettings.iOS.SetiPhoneLaunchScreenType(iOSLaunchScreenType.ImageAndBackgroundRelative);
//    PlayerSettings.iOS.SetLaunchScreenImage(iphone, iOSLaunchScreenImageType.iPhonePortraitImage);
//    PlayerSettings.iOS.SetiPadLaunchScreenType(iOSLaunchScreenType.ImageAndBackgroundRelative);
//    PlayerSettings.iOS.SetLaunchScreenImage(ipad, iOSLaunchScreenImageType.iPadImage);
//}
// using System;
// using UnityEditor;
// using UnityEngine;
// using Orgil;
// [CustomPropertyDrawer(typeof(DrawIfAttribute))]
// public class DrawIfPropertyDrawer : PropertyDrawer {
//     DrawIfAttribute attr;
//     SerializedProperty comparedField;
//     private float propertyHeight;
//     bool IsNumTp(Type t) { return IsI(t) || IsL(t) || IsF(t) || IsD(t); }
//     bool IsI(Type t) { return t.IsTp<sbyte>() || t.IsTp<byte>() || t.IsTp<short>() || t.IsTp<ushort>() || t.IsTp<int>() || t.IsTp<uint>(); }
//     bool IsL(Type t) { return t.IsTp<long>() || t.IsTp<ulong>(); }
//     bool IsF(Type t) { return t.IsTp<float>(); }
//     bool IsD(Type t) { return t.IsTp<double>() || t.IsTp<decimal>(); }
//     object GetValue(Type t, SerializedProperty sp) {
//         object res = null;
//         if (t.IsTp<AnimationCurve>()) res = sp.animationCurveValue;
//         else if (t.IsTp<bool>()) res = sp.boolValue;
//         else if (t.IsTp<BoundsInt>()) res = sp.boundsIntValue;
//         else if (t.IsTp<Bounds>()) res = sp.boundsValue;
//         else if (t.IsTp<Color>()) res = sp.colorValue;
//         else if (IsD(t)) res = sp.doubleValue;
//         else if (IsF(t)) res = sp.floatValue;
//         else if (IsI(t) || t.IsTp<char>()) res = sp.intValue;
//         else if (IsL(t)) res = sp.longValue;
//         else if (t.IsTp<object>()) res = sp.objectReferenceValue;
//         else if (t.IsTp<Quaternion>()) res = sp.quaternionValue;
//         else if (t.IsTp<RectInt>()) res = sp.rectIntValue;
//         else if (t.IsTp<Rect>()) res = sp.rectValue;
//         else if (t.IsTp<string>()) res = sp.stringValue;
//         else if (t.IsTp<Vector2Int>()) res = sp.vector2IntValue;
//         else if (t.IsTp<Vector2>()) res = sp.vector2Value;
//         else if (t.IsTp<Vector3Int>()) res = sp.vector3IntValue;
//         else if (t.IsTp<Vector3>()) res = sp.vector3Value;
//         else if (t.IsTp<Vector4>()) res = sp.vector4Value;
//         return res;
//     }
//     public override float GetPropertyHeight(SerializedProperty property, GUIContent label) { return propertyHeight; }
//     public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
//         attr = attribute as DrawIfAttribute;
//         object value = GetValue(attr.type, property.serializedObject.FindProperty(attr.name));
//         bool active = false;
//         if (attr.compTp == CompTp.Or) {
//             for (int i = 0; i < attr.values.Length; i++)
//                 if (value.Equals(attr.values[i])) {
//                     active = true;
//                     break;
//                 }
//         } else if (attr.compTp == CompTp.Equals) {
//             if (value.Equals(attr.value))
//                 active = true;
//         } else if (attr.compTp == CompTp.NotEqual) {
//             if (!value.Equals(attr.value))
//                 active = true;
//         } else if (IsNumTp(attr.type)) {
//             double a = 0, b = 0;
//             if (IsI(attr.type)) {
//                 a = (int)value;
//                 b = (int)attr.value;
//             } else if (IsL(attr.type)) {
//                 a = (long)value;
//                 b = (long)attr.value;
//             } else if (IsF(attr.type)) {
//                 a = (float)value;
//                 b = (float)attr.value;
//             } else if (IsD(attr.type)) {
//                 a = (double)value;
//                 b = (double)attr.value;
//             }
//             switch (attr.compTp) {
//                 case CompTp.GreaterThan: if (a > b) active = true; break;
//                 case CompTp.SmallerThan: if (a < b) active = true; break;
//                 case CompTp.SmallerOrEqual: if (a <= b) active = true; break;
//                 case CompTp.GreaterOrEqual: if (a >= b) active = true; break;
//             }
//         }
//         propertyHeight = base.GetPropertyHeight(property, label);
//         if (active) {
//             EditorGUI.PropertyField(position, property);
//         } else {
//             if (attr.disableTp == DisableTp.ReadOnly) {
//                 GUI.enabled = false;
//                 EditorGUI.PropertyField(position, property);
//                 GUI.enabled = true;
//             } else {
//                 propertyHeight = 0f;
//             }
//         }
//     }
// }