using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;
//using GameAnalyticsSDK;
//using Facebook.Unity;
//using Lofelt.NiceVibrations;

[System.Serializable]
public class Sound {
    public string name;
    public AudioClip clip;
    [Range(0f, 1f)] public float volume;
    [Range(0.3f, 3f)] public float pitch;
    public bool loop;
    [HideInInspector] public AudioSource src;
    [HideInInspector] public float tm = 0;
    public static void Play(string name, bool isCheck = true) {
        if (Gc.IsSound) {
            var s = Mn._.sounds.Find(s => s.name == name);
            if (s.NotNull() && (isCheck ? s.tm + s.clip.length < Time.time : true)) {
                s.tm = Time.time;
                s.src.Play();
            }
        }
    }
}
public class Mn : Singleton<Mn> { // Manager
    public List<Sound> sounds;
    void Awake() {
        foreach (Sound s in sounds) {
            s.src = go.Ac<AudioSource>();
            s.src.clip = s.clip;
            s.src.volume = s.volume;
            s.src.pitch = s.pitch;
            s.src.loop = s.loop;
        }
        //GameAnalytics.Initialize();
        //if (!FB.IsInitialized) FB.Init(InitCallback, OnHideUnity);
        //else FB.ActivateApp();
    }
    //void InitCallback() { if (FB.IsInitialized) FB.ActivateApp(); }
    //void OnHideUnity(bool isGameShown) { Time.timeScale = isGameShown ? 1 : 0; }
}
public class Ga {
    static void Event(int i) {
        //GameAnalytics.NewProgressionEvent(i == 0 ? GAProgressionStatus.Start : i > 0 ? GAProgressionStatus.Complete : GAProgressionStatus.Fail, (i == 0 ? "Start" : i > 0 ? "Complete" : "Fail") + "-" + Gc.Level, (int)Gc.Score);
    }
    public static void Start() { Event(0); }
    public static void Complete() {
        Sound.Play("Complete");
        Event(1);
    }
    public static void Fail() {
        Event(-1);
        Sound.Play("Fail");
    }
}
public class Vib {
    public static void I(int tp) {
        // if (Gc.IsHaptic) HapticPatterns.PlayPreset((HapticPatterns.PresetType)tp);
    }
    public static void I(float amp, float frq) {
        // if (Gc.IsHaptic) HapticPatterns.PlayEmphasis(amp, frq);
    }
    public static void I(float amp, float frq, float dur) {
        // if (Gc.IsHaptic) HapticPatterns.PlayConstant(amp, frq, dur);
    }
    public static void Selection() { I(0); }
    public static void Success() { I(1); }
    public static void Warning() { I(2); }
    public static void Failure() { I(3); }
    public static void Light() { I(4); }
    public static void Medium() { I(5); }
    public static void Heavy() { I(6); }
    public static void Rigid() { I(7); }
    public static void Soft() { I(8); }
}