﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class Player : Character {
    public static Player _ { get { if (!__) __ = (Player)FindObjectOfType(typeof(Player)); return __; } }
    static Player __;
    Vector2 dMp => V2.V((Mp.x - mp.x) / (Screen.width * 0.3f), (Mp.y - mp.y) / (Screen.width * 0.2f));
    public int startAnimal = 10;
    public float removeTailTime = 1, doubleTapTime = 1;
    float clkTm = -1000, spdTm = 0;
    public Text speedUpTxt;
    bool isTutorial = false;
    void Start() {
        speedUpTxt.Hide();
    }
    public void Tutorial() {
        if (!isTutorial && tailCount >= startAnimal + 5 && Gc.Level <= 3) {
            isTutorial = true;
            speedUpTxt.Show();
            Invoke("TutorialHide", 5.5f);
        }
    }
    void TutorialHide() {
        if (isTutorial && speedUpTxt.ActS())
            speedUpTxt.Hide();
    }
    public void Init() {
        body = new Tail(go, TailTp.Body);
    }
    void Update() {
        if (IsGame) {
            if (IsMbD) {
                mp = Mp;
#if UNITY_EDITOR
                if (clkTm + doubleTapTime >= Time.time && tailCount > startAnimal)
                    SpeedUp(true);
#else
                if (Input.GetTouch(0).tapCount >= 2 && tailCount > startAnimal)
                    SpeedUp(true);
#endif
                clkTm = Time.time;
            }
            if (IsMb && V3.Dis(Mp, mp) > 10)
                Tr = Q.Y(Ang.Xy(mp, Mp));
            if (IsMbU)
                SpeedUp(false);
            rb.V(TfF * spd);
            if (isSpdUp) {
                spdTm += Dt;
                if (spdTm > removeTailTime) {
                    spdTm -= removeTailTime;
                    Tail tail;
                    if (tails.Count > 0) {
                        tail = tails.Last();
                        tails.Remove(tail);
                    } else if (bodies.Count > 0) {
                        tail = bodies.Last();
                        bodies.Remove(tail);
                    } else {
                        tail = heads.Last();
                        heads.Remove(tail);
                    }
                    AddScr(tail.Tp, ScrTp.RmvTail);
                    tail.Upd(null);
                }
                if (tailCount <= startAnimal)
                    SpeedUp(false);
            }
        }
    }
    public void MbD() {
        mp = Mp;
    }
    void SpeedUp(bool isUp) {
        if (isUp) TutorialHide();
        spdUp = isUp ? 2 : 1;
        go.Child(1).PsEmRot(isUp ? 100 : 0);
    }
    public void Done(Character killer = null) {
        var win = killer ? killer : CC.chars[0];
        foreach (var chr in CC.chars) {
            chr.rb.V0();
            chr.body.Anim(chr == win ? AnimTp.Win : chr.lb.isLive ? AnimTp.Idle : AnimTp.Death);
        }
        foreach (var t in GC.tails)
            t.Anim(t.par == win ? AnimTp.Win : AnimTp.Idle);
    }
    public void Revive() {
    }
}