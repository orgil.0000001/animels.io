﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public enum GameState { Menu, Game, Complete, Fail, Settings }
public enum Data { IsWin, IsHaptic, IsSound, Level, Coin, Best, LevelData, Settings, BtnLvls, Player, Time, Skin, SkinCnt }
public class Gc : Singleton<Gc> { // Game Controller
    public static GameState State;
    public static bool IsWin { get => Data.IsWin.B(); set => Data.IsWin.B(value); }
    public static bool IsHaptic { get => Data.IsHaptic.B(); set => Data.IsHaptic.B(value); }
    public static bool IsSound { get => Data.IsSound.B(); set => Data.IsSound.B(value); }
    public static bool Settings { get => Data.Settings.B(); set => Data.Settings.B(value); }
    public static int Level { get => Data.Level.I(); set => Data.Level.I(value); }
    public static int Coin { get => Data.Coin.I(); set { Data.Coin.I(value); CC.coinTxt.text = "" + value; } }
    public static int SkinCnt { get => Data.SkinCnt.I(); set => Data.SkinCnt.I(value); }
    public static float Best { get => Data.Best.F(); set => Data.Best.F(value); }
    public static float Score, Tm;
    // bool, int, float, string, Vector2, Vector2Int, Vector3, Vector3Int, Vector4
    public static Dictionary<Data, object> Datas = new Dictionary<Data, object>() {
        { Data.Level, 1 }, { Data.Coin, 0 }, { Data.Best, 0f }, { Data.Time, 0 }, { Data.SkinCnt, 0 },
        { Data.IsWin, true }, { Data.IsHaptic, true }, { Data.IsSound, true }, { Data.Settings, false },
        { Data.LevelData, "" }, { Data.BtnLvls, "" }, { Data.Player, "Player" }, { Data.Skin, ""}
    };
    public float groundSize = 300, borderSize = 1;
    public int lvl = 0;
    [System.Serializable]
    public class Env {
        public Texture groundTex;
        public Color animalCol, woodCol, effectCol;
    }
    public List<Env> envs;
    [HideInInspector] public List<Tail> tails = new List<Tail>();
    public Material tailMat, woodMat;
    [HideInInspector] public GameObject eatEffectPf;
    public GameObject animGo;
    public GameObject houseGo;
    Dictionary<int, List<GameObject>> houseDic = new Dictionary<int, List<GameObject>>();
    public List<House> houses = new List<House>();
    public List<GameObject> btnEffectPfs;
    void Awake() {
        IO.Init();
        Score = 0;
        Tm = 120;
        if (lvl > 0)
            Level = lvl;
    }
    void Start() {
        Bot.NearCount = M.RemapI(Level, 1, 20, 10, 1);
        int maxHouseSz = M.RemapI(Level, 1, 20, 20, 26);
        Tail.animal = V2I.V((Level - 1) % 3, 4);
        Tail.Limit = V3.pzp * (groundSize / 2 - 1);
        animGo.ChildGo(1, 0, Tail.animal.x).An().Play("Run");
        animGo.ChildGo(2, 0, Tail.animal.x).An().Play("Jump");
        animGo.ChildGo(3, 0, Tail.animal.x).An().Play("Death");
        Tail.AnimDic = new Dictionary<AnimTp, Tail> {
            { AnimTp.Idle, new Tail(animGo.ChildGo(0), TailTp.Tail) },
            { AnimTp.Run, new Tail(animGo.ChildGo(1), TailTp.Tail) },
            { AnimTp.Win, new Tail(animGo.ChildGo(2), TailTp.Tail) },
            { AnimTp.Death, new Tail(animGo.ChildGo(3), TailTp.Tail) },
        };
        P.Init();
        var tfs = go.Child(0).Childs();
        float x = (groundSize + borderSize) / 2, x2 = groundSize / 2 - 2, houseX = groundSize / 2 - 10;
        tfs[0].Tls(groundSize, 1, groundSize);
        int env = (Level - 1) % envs.Count;
        tfs[0].RenMat().mainTexture = envs[env].groundTex;
        tfs[0].RenMat().SetTextureScale("_MainTex", V2.V(groundSize / 5));
        tailMat.color = envs[env].animalCol;
        woodMat.color = envs[env].woodCol;
        eatEffectPf = O.LoadGo("EatEffect");
        eatEffectPf.PsMainStaC(Ps.Mmg(envs[env].effectCol));
        P.Child(1).PsMainStaC(Ps.Mmg(envs[env].effectCol));
        tfs[1].localScale = tfs[2].localScale = V3.V(groundSize, 100, borderSize);
        tfs[3].localScale = tfs[4].localScale = V3.V(groundSize + borderSize * 2, 100, borderSize);
        tfs[1].TlpX(-x);
        tfs[2].TlpX(x);
        tfs[3].TlpZ(-x);
        tfs[4].TlpZ(x);
        Transform botsTf = go.Child(2), tailsTf = go.Child(3), housesTf = go.Child(4), btnEffectsTf = go.Child(5);
        Bot botPf = O.Load<Bot>("Bot");
        GameObject tailPf = O.LoadGo("Tail");
        P.Lb(true);
        houseGo.ChildGos().ForEach(go => {
            int cnt = go.name.I();
            if (!houseDic.ContainsKey(cnt)) houseDic[cnt] = new List<GameObject>();
            houseDic[cnt].Add(go);
        });
        CrtRndPnts(20, 18, (-houseX, houseX), 0, (-houseX, houseX), P.Tp).ForEach(p => {
            houses.Add(new House(houseDic[Rnd.RngIn(2, maxHouseSz)].Rnd(), p, housesTf, tailPf, tailsTf));
        });
        int effectIdx = 0;
        CrtRndPnts(20, 24, (-houseX, houseX), 0, (-houseX, houseX), P.Tp).ForEach(p => {
            GameObject btnGo = Ins(btnEffectPfs[effectIdx++ % 4], p, Q.O, btnEffectsTf);
            btnGo.ScR(1);
            btnGo.Child(0, 0).Tls(V3.V(2, .1f, 2));
            btnGo.Child(0, 1).Tls(V3.V(.7f, .7f, 2));
            btnGo.Child(1).Tls(V3.V(2.3f, .01f, 2.3f));
            int i = 0;
            btnGo.Child(2).Childs().ForEach(tf => {
                tf.Tlp(Ang.V2(i++ * 60f, 5).Xz());
                tf.Tls(tf.Tls() * 2);
            });
            CrtRndPnts(10, 1.5f, (p.x - 5, p.x + 5), 0, (p.z - 5, p.z + 5), P.Tp).ForEach(p => {
                tails.Add(new Tail(Ins(tailPf, p, Rnd.Ry, tailsTf), TailTp.Tail));
            });
        });
        CrtRndPnts(10, 10, (-x2, x2), 0, (-x2, x2), P.Tp).ForEach(p => {
            Bot bot = Ins(botPf, p, Rnd.Ry, botsTf);
            bot.Lb(false);
            bot.Child(0, Tail.animal.x, Tail.animal.y).RenMatCol(bot.lb.col);
        });
        float spc = 1.2f;
        for (int i = P.startAnimal; i > 0; i--) {
            for (int j = 0; j < Character.TailOffset; j++)
                P.pnts.Insert(0, (P.Tp - V3.Z((i - j.F() / Character.TailOffset) * spc), Q.O));
            Tail tail = new Tail(Ins(tailPf, P.Tp - V3.Z(i * spc), Q.O, tailsTf), TailTp.Tail);
            tail.Upd(P, false);
            P.bodies.Insert(0, tail);
            tails.Add(tail);
        }
        CrtRndPnts(1000, 4, (-x2, x2), 0, (-x2, x2), P.Tp).ForEach(p => {
            CrtRndPnts(Rnd.RngIn(1, 5), 1, (p.x - 2, p.x + 2), 0, (p.z - 2, p.z + 2), P.Tp).ForEach(p => {
                if (tails.Count < 1000)
                    tails.Add(new Tail(Ins(tailPf, p, Rnd.Ry, tailsTf), TailTp.Tail));
            });
        });
        CC.Menu();
    }
    void Update() {
        CC.chars.ForEach(c => c.body.Animate());
        tails.ForEach(t => {
            t.Animate();
            t.Update();
        });
    }
    List<Vector3> CrtRndPnts(int n, float dis, (float, float) x, float y, (float, float) z, params Vector3[] init) {
        List<Vector3> res = init.Lis();
        for (int i = 0; i < 10000 && res.Count < init.Length + n; i++) {
            Vector3 p = V3.V(Rnd.Rng(x.Item1, x.Item2), y, Rnd.Rng(z.Item1, z.Item2));
            if (res.FindIndex(0, res.Count, x => V3.Dis(x, p) < dis) < 0)
                res.Add(p);
        }
        return res.Rng(init.Length, res.Count - 1);
    }
    void Fog(Color col = default, float dis = 80, float spc = 10) {
        if (col.IsDef())
            col = Cam.backgroundColor;
        RenderSettings.fog = true;
        RenderSettings.fogColor = Cam.backgroundColor = col;
        RenderSettings.fogMode = FogMode.Linear;
        RenderSettings.fogStartDistance = dis;
        RenderSettings.fogEndDistance = dis + spc;
    }
    public static int GetLvl(int lvl, int lvlCnt, int rndSta, int rndEnd) {
        if (Data.LevelData.S().IsNe())
            Data.LevelData.ListI(O.LisAp(1, 1, lvlCnt));
        List<int> lis = Data.LevelData.ListI();
        if (lvl <= lis.Count) {
            return lis[lvl - 1];
        } else {
            int prvLvl = lis.Last();
            for (int i = lis.Count + 1; i <= lvl; i++) {
                prvLvl = prvLvl == rndSta ? Rnd.RngIn(rndSta + 1, rndEnd) : prvLvl == rndEnd ? Rnd.RngIn(rndSta, rndEnd - 1) : Rnd.P((prvLvl - rndSta).F() / (rndEnd - rndSta)) ? Rnd.RngIn(rndSta, prvLvl - 1) : Rnd.RngIn(prvLvl + 1, rndEnd);
                lis.Add(prvLvl);
            }
            Data.LevelData.ListI(lis);
            return prvLvl;
        }
    }
}
public class House {
    public GameObject go;
    public List<Vector3> columns = new List<Vector3>();
    public List<Tail> tails = new List<Tail>();
    public House(GameObject pf, Vector3 p, Transform housesTf, GameObject tailPf, Transform tailsTf) {
        go = Mb.Ins(pf.ChildGo(0), p, Q.O, housesTf);
        go.Childs().ForEach(tf => {
            if (tf.localPosition.y < 1.5f)
                columns.Add(tf.position);
        });
        pf.Child(1).Childs().ForEach(tf => {
            Tail tail = new Tail(Mb.Ins(tailPf, go.TfPnt(tf.localPosition - V3.Y(.4f)), Rnd.Ry, tailsTf), TailTp.House);
            tails.Add(tail);
            Gc._.tails.Add(tail);
        });
    }
    public void Eat() {
        var gos = go.ChildGos();
        Vector3 p = V3.O;
        gos.ForEach(cGo => p += cGo.Tp());
        p /= gos.Count;
        gos.ForEach(cGo => {
            cGo.Bc(GcTp.Add);
            cGo.Rb(GcTp.Add).AddExplosionForce(1000, p, 100);
            Mb.Dst(cGo, 10);
        });
        Gc._.houses.Remove(this);
    }
}