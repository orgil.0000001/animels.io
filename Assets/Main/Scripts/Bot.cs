using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Bot : Character {
    public static int NearCount;
    float curAng, ang, t = 0, dt = 0;
    void Start() {
        body = new Tail(go, TailTp.Body);
        curAng = ang = Te.y;
    }
    public Tail target = null;
    void Update() {
        body.Update();
        if (IsGame && IsLive) {
            if (target.Null() || target.par) {
                var l = GC.tails.FindAll(t => !t.par && !t.isHouse);
                if (l.IsCount()) {
                    List<(int, float)> dis = new List<(int, float)>();
                    for (int i = 0; i < l.Count; i++)
                        dis.Add((i, V3.Dis(l[i].Tp, Tp)));
                    dis.Sort((x, y) => x.Item2.CompareTo(y.Item2));
                    target = l[dis.Rng(0, M.Min(NearCount, l.Count)).Rnd().Item1];
                }
            }
            if (target.NotNull()) {
                Tr = Q.Lerp(Tr, Q.Y(Ang.Xz(Tp, target.Tp)), Dt * 3);
            } else {
                dt += Dt;
                if (dt > t) {
                    dt = 0;
                    t = Rnd.Rng(1.5f, 3);
                    ang = Rnd.Ang;
                }
                curAng = M.Lerp(curAng, ang, Dt * 5);
                Tr = Q.Y(curAng);
            }
            rb.V(TfF * spd);
        }
    }
}
