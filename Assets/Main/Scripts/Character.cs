using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class LbData {
    public string name;
    public int scr, scr2, kill;
    public Color col;
    public bool isLive, isPlayer;
    public LbData(string name, Color col, bool isPlayer) { Set(name, 0, 0, 0, col, true, isPlayer); }
    public void Set(string name, int scr, int scr2, int kill, Color col, bool isLive, bool isPlayer) {
        this.name = name;
        this.scr = scr;
        this.scr2 = scr2;
        this.kill = kill;
        this.col = col;
        this.isLive = isLive;
        this.isPlayer = isPlayer;
    }
}
public class Character : Mb {
    public LbData lb;
    public bool IsLive => lb.isLive;
    public bool IsPlayer => lb.isPlayer;
    bool isStart = false;
    public List<(Vector3, Quaternion)> pnts = new List<(Vector3, Quaternion)>();
    [HideInInspector] public List<Tail> tails = new List<Tail>(), bodies = new List<Tail>(), heads = new List<Tail>();
    public static int TailOffset = 15;
    public float spd => initSpd * spdUp;
    protected float spdUp = 1;
    float initSpd;
    public bool isSpdUp => spdUp > 1;
    [HideInInspector] public int tailCount => heads.Count + bodies.Count + tails.Count;
    public Tail body;
    public void Lb(bool isPlayer) {
        initSpd = isPlayer ? 5 : 4.5f;
        lb = isPlayer ? new LbData(Data.Player.S(), O.Cols[0], true) : new LbData(O.Names.Rnd(), O.Cols.RndI(0), false);
    }
    void RmvTail(ref List<Tail> l) {
        for (int i = 0; i < l.Count; i++)
            if (l[i].par != this) {
                l.RmvAt(i);
                i--;
            }
    }
    public void RmvTails() {
        RmvTail(ref heads);
        RmvTail(ref bodies);
        RmvTail(ref tails);
    }
    void FixedUpdate() {
        if (IsGame && IsLive) {
            if (!isStart) {
                isStart = true;
                body.Anim(AnimTp.Run);
                bodies.ForEach(t => t.Anim(AnimTp.Run));
            }
            RmvTails();
            if (isSpdUp)
                pnts.Insert(0, (V3.Lerp(Tp, pnts[0].Item1, .5f), Q.Lerp(Tr, pnts[0].Item2, .5f)));
            pnts.Insert(0, (Tp, Tr));
            if (heads.IsCount()) {
                if ((pnts.Count - 1) % TailOffset == 0) {
                    while (bodies.Count < pnts.Count / TailOffset - 1 && heads.IsCount()) {
                        bodies.Insert(0, heads[0]);
                        heads.RmvAt(0);
                    }
                }
                while (pnts.Count > (bodies.Count + heads.Count + 1) * TailOffset + 1)
                    pnts.RmvAtLast();
                for (int i = 0; i < bodies.Count; i++)
                    bodies[i].Move(pnts.CIdx(pnts.Count - 1 - (bodies.Count - i) * TailOffset));
                for (int i = 0; i < heads.Count; i++)
                    heads[i].Move(pnts.CIdx((i + 1) * TailOffset));
            } else {
                if (tails.IsCount() && (pnts.Count - 1) % TailOffset == 0) {
                    while (bodies.Count < pnts.Count / TailOffset - 1 && tails.IsCount()) {
                        bodies.Add(tails[0]);
                        tails.RmvAt(0);
                    }
                }
                List<Tail> l = bodies.Copy().Add(tails);
                while (pnts.Count > (l.Count + 1) * TailOffset + 1)
                    pnts.RmvAtLast();
                for (int i = 0; i < l.Count; i++)
                    l[i].Move(pnts.CIdx((i + 1) * TailOffset));
            }
        }
    }
    Bot bot = null;
    IEnumerator btnEffectCor = null;
    IEnumerator BtnEffectCor(GameObject btnGo) {
        List<Vector3> pnts = new List<Vector3>();
        for (int i = 0; i < 360; i += 30)
            pnts.Add(btnGo.Tp() + Ang.V2(i, 6).Xz());
        Sound.Play(btnGo.name.Split("(")[0]);
        EatCircle(pnts);
        btnGo.ChildHide(2);
        btnGo.ChildShow(2);
        yield return Wf.Sec(2);
        btnGo.ChildHide(2);
    }
    void OnTriggerEnter(Collider other) {
        if (other.Tag(Tag.Button) && IsGame && IsLive && IsPlayer) {
            if (btnEffectCor.NotNull()) StopCor(btnEffectCor);
            StaCor(btnEffectCor = BtnEffectCor(other.gameObject));
        } else if (other.Tag(Tag.Tail) && IsGame && IsLive) {
            Tail tail = GC.tails.Find(t => t.tf == other.transform);
            if (!tail.par && !IsPlayer && !tail.isHouse) {
                tail.Upd(this);
                heads.Insert(0, tail);
                AddScr(tail.Tp, ScrTp.Eat);
                if (!IsPlayer) {
                    if (!bot) bot = go.Gc<Bot>();
                    bot.target = null;
                }
            } else if (bodies.Contains(tail)) {
                var pnts = bodies.Rng(0, bodies.IndexOf(tail) + 1).Parse(x => x.Tp);
                if (pnts.Count < 3) return;
                EatCircle(pnts);
            }
        }
    }
    void EatCircle(List<Vector3> pnts) {
        List<Tail> l = tails.Copy().Add(bodies).Add(heads), l2 = new List<Tail>();
        List<Character> chars = new List<Character>();
        bool isEat = false;
        foreach (var chr in CC.chars) {
            if (chr != this && chr.IsLive && IsPointInPolygon(chr.Tp, pnts) &&
                (IsPlayer ? chr.body.isVisible : chr.IsPlayer ? body.isVisible : true)) {
                chr.lb.isLive = false;
                lb.kill++;
                chr.rb.V0();
                chr.body.Anim(AnimTp.Death);
                chars.Add(chr);
                l2 = l2.Add(chr.tails.Copy().Add(chr.bodies).Add(chr.heads));
                isEat = true;
                Sound.Play("Score2");
                if (chr.IsPlayer) {
                    P.Done(this);
                    CC.Fail();
                }
            }
        }
        foreach (var t in GC.tails) {
            if (!l.Contains(t) && IsPointInPolygon(t.Tp, pnts) && !t.isHouse
            /* && (IsPlayer ? t.isVisible : true)*/) {
                if (t.par && t.par.IsPlayer ? t.par.tailCount > P.startAnimal : true) {
                    AddScr(t.Tp, t.par ? ScrTp.Circle : ScrTp.Eat);
                    if (t.par) AddScr(t.Tp, ScrTp.RmvCircle);
                    t.Upd(this);
                    // tails.Add(t);
                    heads.Insert(0, t);
                    isEat = true;
                }
            } else if (l2.Contains(t)) {
                t.Upd(null);
            }
        }
        chars.ForEach(chr => AddScr(chr.Tp, ScrTp.Kill));
        List<House> houses = new List<House>();
        if (IsPlayer) {
            foreach (var h in GC.houses)
                if (!h.columns.Parse(p => IsPointInPolygon(p, pnts)).Contains(false))
                    houses.Add(h);
            houses.ForEach(h => {
                h.tails.ForEach(t => {
                    AddScr(t.Tp, ScrTp.Circle);
                    t.tp = TailTp.Tail;
                    t.Upd(this);
                    heads.Insert(0, t);
                });
                h.Eat();
                isEat = true;
            });
        }
        if (IsPlayer && isEat)
            StaCor(EatEffectCor(pnts.Lis(), chars.IsCount() ? "Kill" : houses.IsCount() ? "House" : "Score", 2));
    }
    IEnumerator EatEffectCor(List<Vector3> pnts, string sound, float dur = 1) {
        Sound.Play(sound);
        P.Tutorial();
        pnts = Crv.Crs(pnts, 10, CrsTp.Loop);
        pnts.RmvAt(0);
        var vs = pnts.Copy().Add(pnts.Parse(p => p + V3.Y(4)));
        var ts = new List<int>();
        var uv = new List<Vector2>();
        for (int i = 0; i < pnts.Count; i++)
            ts.Fc(i, pnts.Count + i, pnts.Count + (i + 1) % pnts.Count, (i + 1) % pnts.Count);
        for (int i = 0; i < vs.Count; i++)
            uv.Add(V2.V(i % pnts.Count / (pnts.Count - 1f), i / pnts.Count));
        Msh msh = new Msh(vs, ts, uv, true);
        Mesh mesh = new Mesh();
        msh.Upd(ref mesh);
        var effectGo = Ins(GC.eatEffectPf, V3.O, Q.O);
        effectGo.Hide();
        effectGo.PsMainStaLt(Ps.Mmc(dur), 1);
        var psr = effectGo.Psr();
        psr.mesh = mesh;
        var mat = psr.material;
        effectGo.Show();
        yield return Cor(t => mat.SetTextureOffset("_MainTex", V2.V(t * 10, 0)), dur);
        Destroy(effectGo);
    }
    public enum ScrTp { Eat, Circle, Kill, RmvTail, RmvCircle }
    public void AddScr(Vector3 p, ScrTp tp) {
        if (IsPlayer && tp <= ScrTp.RmvTail) {
            if (tp == ScrTp.RmvTail) CC.RiseTxt(false, P.Tp + V3.Y(2), "-1", .3f, 1.5f, .6f);
            if (tp == ScrTp.Kill) CC.RiseTxt(true, P.Tp + V3.Y(3), "KILL", .3f, 3, .8f);
            else CC.RiseTxt(false, p, tp == ScrTp.Circle ? "+3" : "+1", .3f, 1.5f, .6f);
            Gc.Coin += tp == ScrTp.Kill ? 50 : tp == ScrTp.Circle ? 3 : 1;
        }
        lb.scr += tp == ScrTp.Kill ? 50 : tp == ScrTp.Circle ? 3 : tp == ScrTp.Eat ? 1 : -1;
    }
    public bool IsPointInPolygon(Vector3 point, List<Vector3> pnts) {
        bool inside = false;
        for (int i = 0; i < pnts.Count; i++) {
            Vector3 sta = pnts.RepIdx(i - 1), end = pnts[i];
            inside ^= (end.z > point.z ^ sta.z > point.z) /* ? point.z inside [sta.z;end.z] segment ? */
                && /* if so, test if it is under the segment */
                ((point.x - end.x) < (point.z - end.z) * (sta.x - end.x) / (sta.z - end.z));
        }
        return inside;
    }
}