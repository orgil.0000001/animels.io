using System.IO;
using System.Linq;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace Orgil {
    public class IO {
        static string NmSpc = "Orgil", CurPath = Directory.GetCurrentDirectory() + "/", ToolsPath = CurPath + "Assets/Main/Scripts/Tools/";
        public static bool IsDirExists(string path) { return Directory.Exists(path); }
        public static string[] GetDirs(string path) { return Directory.GetDirectories(path); }
        public static void CrtDir(string path) { Directory.CreateDirectory(path); }
        public static void CheckCrtDir(string path) { if (!IsDirExists(path)) CrtDir(path); }
        public static bool IsFileExists(string path) { return File.Exists(path); }
        public static string[] GetFiles(string path) { return Directory.GetFiles(path); }
        public static string[] GetFiles(string path, params string[] exts) { return Directory.GetFiles(path, "*.*", SearchOption.AllDirectories).Where(s => exts.Contains(Path.GetExtension(s))).Arr(); }
        public static void CrtFile(string path, string data) { File.WriteAllText(path, data); }
        public static string ReadFile(string path) { try { return File.ReadAllText(path); } catch (FileNotFoundException) { return ""; } }
        public static void FileRplStr(string path, string regex, string replace) { CrtFile(path, Rgx.Rpl(ReadFile(path), regex, replace)); }
        public static void CheckMeta(string path, params string[] a) {
            for (int i = 0; i < a.Length; i += 2)
                if (!ReadFile(CurPath + path).Contains(a[i] + ": " + a[i + 1]))
                    FileRplStr(CurPath + path, a[i] + ":.+", a[i] + ": " + a[i + 1]);
        }
        public static void SetMeta(string path, params string[] a) {
            for (int i = 0; i < a.Length; i += 2)
                FileRplStr(CurPath + path, a[i] + ":.+", a[i] + ": " + a[i + 1]);
        }
        public static string RoVar(string comment, string type, string name, string value) { return (comment == "" ? "" : "\t///<summary>" + comment + "</summary>\n") + "\tpublic static readonly " + type + " " + name + " = " + value + ";\n\n"; }
        public static string Func(string comment, bool isComSum, bool isPublic, bool isStatic, string retType, string funcName, string args, int tab, bool isEnter, int lastEnter, params string[] lines) { return AddTab((comment == "" ? "" : isComSum ? "///<summary>" + comment + "</summary>\n" : "// " + comment + "\n") + (isPublic ? "public " : "") + (isStatic ? "static " : "") + retType + " " + funcName + "(" + args + ") {" + (isEnter ? lines.S("\n\t", "\n\t", "\n") : lines.S(" ", " ", " ")) + "}", tab) + '\n'.N(lastEnter + 1); }
        public static string AddTab(string file, int n = 1) { return file.Split('\n').S("\n" + '\t'.N(n), '\t'.N(n)); }
        public static string CrtClass(bool isNmSpc, bool isStatic, bool isPartial, string name, string body, params string[] namespaces) {
            string res = "", data = "public " + (isStatic ? "static " : "") + (isPartial ? "partial " : "") + "class " + name + " {\n" + body + "}";
            if (namespaces.NotNull())
                namespaces.Lis().ForEach(x => res += "using " + x + ";\n");
            return (res == "" ? "" : res + "\n") + (isNmSpc ? "namespace " + NmSpc + " {\n" + AddTab(data) + "\n}" : data);
        }
        public static void Init() {
            Application.targetFrameRate = 60;
#if UNITY_EDITOR
            // Set Game View Scale
            System.Type tp = typeof(UnityEditor.EditorWindow).Assembly.GetType("UnityEditor.GameView");
            var area = tp.GetField("m_ZoomArea", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).GetValue(UnityEditor.EditorWindow.GetWindow(tp));
            area.GetType().GetField("m_Scale", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).SetValue(area, V2.V(0.1f));
            // Create gitignore
            if (!IsFileExists(".gitignore"))
                CrtFile(".gitignore", ReadFile(ToolsPath + "gitignore"));
            // Tag Layer
            string[] lyrArr = lyrArr = UnityEditorInternal.InternalEditorUtility.layers;
            string tag = UnityEditorInternal.InternalEditorUtility.tags.RmvSpcStr(), lyr = "", lm = "";
            if (tag != O.EnumStrArr<Tag>().S() || lyrArr.S() != typeof(Lyr).GetFields().Select(x => x.Name).Arr().RmvSpcStr()) {
                foreach (string name in lyrArr) {
                    lyr += name.RmvSpc() + " = " + name.NameLyr() + ", ";
                    lm += name.RmvSpc() + " = " + name.NameLm() + ", ";
                }
                CrtFile(ToolsPath + "TagLayer.cs", "namespace " + NmSpc + " {\n\tpublic enum Tag { " + tag + " }\n\tpublic class Lyr { public const int " + lyr.SubLast(0, 2) + "; }\n\tpublic class Lm { public const int " + lm.SubLast(0, 2) + "; }\n}");
            }
            ExtGo();
#endif
        }
        static void ExtGo() {
            string tmpPath = ToolsPath + "ExtGo.txt", readPath = ToolsPath + "ExtGo.cs", crtPath = ToolsPath + "ExtComCol.cs", readData = ReadFile(readPath);
            if (readData != ReadFile(tmpPath)) {
                CrtFile(tmpPath, readData);
                string data = "", line;
                bool isStart = false;
                StreamReader file = new StreamReader(readPath);
                while ((line = file.ReadLine()).NotNull()) {
                    if (isStart) {
                        line = line.Trim();
                        if (line.IsS("///<summary>")) {
                            data += "\t" + line + "\n";
                        } else if (line.IsS("public static")) {
                            line = line.Sub(0, line.Idx('{') + 1);
                            int idx = line.Idx('(');
                            string[] arr2 = line.Sub(0, idx).Trim().Split(' '), arr3 = line.Sub(idx + 1, line.LastIdx(')') - idx - 1).Split(',');
                            data += "\t" + line.Replace("this GameObject a", "this NAME a") + (arr2[2] == "void" ? "" : " return") + " a.gameObject." + arr2[arr2.Length - 1] + "(";
                            for (int i = 1; i < arr3.Length; i++) {
                                string[] arr4 = arr3[i].Trim().Split(' ');
                                data += (arr4[0] == "params" || arr4[0][0] == '[' ? arr4[2] : arr4[1]) + (i == arr3.Length - 1 ? "" : ", ");
                            }
                            data += "); }\n";
                        }
                    } else if (line.Trim().IsS("public static class"))
                        isStart = true;
                }
                file.Close();
                CrtFile(crtPath, CrtClass(true, true, false, "ExtComCol", data.Replace("NAME", "Component") + data.Replace("NAME", "Collision") + data.Replace("NAME", "Collision2D"), "System.Collections.Generic", "UnityEngine", "UnityEngine.UI", "TMPro"));
            }
        }
    }
}
// static void ExtEnum() {
//     string tmpPath = ToolsPath + "Enum.txt", readPath = ToolsPath + "Enum.cs", crtPath = ToolsPath + "ExtEnum.cs", readData = ReadFile(readPath), data = "", type = "";
//     if (readData != ReadFile(tmpPath)) {
//         CrtFile(tmpPath, readData);
//         string[] arr = readData.RgxRpl("[\n|,]", " ").RgxRpl("[\\{\\}]", " # ").RgxRpl("\\s+", " ").RgxRmv("public enum ").RgxSplitSpc();
//         bool isSta = false;
//         for (int i = 0; i < arr.Length; i++)
//             if (arr[i] == "#") {
//                 isSta = !isSta;
//                 if (isSta)
//                     type = arr[i - 1];
//             } else if (isSta)
//                 data += Func("", false, true, true, "bool", "Is" + arr[i], "this " + type + " a", 1, false, 0, "return a == " + type + "." + arr[i] + ";");
//         CrtFile(crtPath, CrtClass(true, true, false, "ExtEnum", data));
//     }
// }
//public static void EditorMenuItem() {
//    string[] dirs = GetDirs(MainPath + "Resources/UI");
//    List<string> paths = new List<string>();
//    foreach (string dir in dirs)
//        paths.Add(GetFiles(dir, ".prefab"));
//    string data = "";
//    for (int i = 0; i < paths.Count; i++) {
//        string path = paths[i].SubLast(paths[i].IndexOf("Resources") + 10, 7);
//        string[] arr = path.Split('/');
//        string name = arr[1] == arr[2] ? arr[1] : arr[2].Replace(arr[1], "");
//        if (!arr[2].Contains("Stage", "StageLevel"))
//            data += "\t[MenuItem (\"GameObject/" + arr[0] + "/" + arr[1] + "/" + name + "\")]\n" +
//                "\tpublic static void " + arr[2] + " () {\n" +
//                "\t\tCreate" + (arr[2].Contains("Hud", "GameSettings", "CanvasController", "Menu") ? "" : "Prefab") +
//                " (\"" + path + "\", \"" + (arr[1].Contains("Hud", "GameSettings", "UI") ? name : arr[1]) + "\");\n\t}\n\n";
//    }
//    data = CrtClass(false, false, true, "EditorMenuItem : Editor", data, "UnityEditor");
//    CrtFile(ScriptsPath + "Editor/EditorMenuItem.cs", data);
//}
//static string[] charDataArr = new string[] {
//    "                                     ▀ ▀                     ▀▄▀                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ",
//    "▄▀▀▀▄ █▀▀▀▀ █▀▀▀▄ █▀▀▀▀  █▀█  █▀▀▀▀ █▀▀▀▀ █ █ █ ▄▀▀▀▄ █   █ █   █ █  ▄▀  █▀▀█ █▄ ▄█ █   █ ▄▀▀▀▄ ▄▀▀▀▄ █▀▀▀█ █▀▀▀▄ ▄▀▀▀▄ ▀▀█▀▀ █   █ █   █ ▄▀█▀▄ █   █ █  █  █   █ █ █ █ █ █ █ ▀█    █   █ █     ▀▀▀▀▄ █ ▄▀▄ ▄▀▀▀█        ▄▄▄▀                          ▀ ▀                     ▀▄▀                                                                                                                                                  ▄▀▀▀▄ █▀▀▀▄ ▄▀▀▀▄ █▀▀▀▄ █▀▀▀▀ █▀▀▀▀ ▄▀▀▀▄ █   █  ▀█▀    ▀█▀ █  ▄▀ █     █▄ ▄█ █   █ ▄▀▀▀▄ █▀▀▀▄ ▄▀▀▀▄ █▀▀▀▄ ▄▀▀▀▀ ▀▀█▀▀ █   █ █   █ █   █ █   █ █   █ ▀▀▀▀█       █               █         ▄▀▄       █       ▄      ▀   █     ▀█                                               █                                        ▀▄    ▄█   ▄▀▀▀▄ ▀▀▀█▀   ▄█  █▀▀▀▀  ▄▀▀  ▀▀▀▀█ ▄▀▀▀▄ ▄▀▀▀▄ ▄▀▀▀▄              ▄▄▄▀   █   ▄▀▀▀▄  █ █   ▄█▄▄ ██  ▄  ▄▀▄  ▄▀▀▄    ▄     ▄▀   ▀▄           ▄     █▀   ▀█   ▄      ▄▄    ▀█                   ▄  ▄▀▀   ▀▀▄    █    ▄▄    █ █    ▄▀   ▀▄   ▄▀▀▀▄       ",
//    "█▄▄▄█ █▄▄▄  █▄▄▄▀ █      █ █  █▄▄▄  █▄▄▄  ▀▄█▄▀  ▄▄▄▀ █ ▄▀█ █ ▄▀█ █▄▀    █  █ █ █ █ █▄▄▄█ █   █ █▄▄▄█ █   █ █▄▄▄▀ █       █   ▀▄▄▄█  ▀▄▀  ▀▄█▄▀  ▀▄▀  █  █  ▀▄▄▄█ █ █ █ █ █ █  █▄▄  █▄  █ █▄▄▄   ▄▄▄█ █▄█ █ ▀▄▄▄█  ▀▀▀▄ █▄▄▄  █▀▀▀▄ █▀▀▀▀  █▀█  ▄▀▀▀▄ ▄▀▀▀▄ █ █ █ ▀▀▀▀▄ █  ▄█ █  ▄█ █  ▄▀  █▀▀█ █▄ ▄█ █   █ ▄▀▀▀▄ ▄▀▀▀▄ █▀▀▀█ █▀▀▀▄ ▄▀▀▀▀ ▀▀█▀▀ █   █ █   █ ▄▀█▀▄ ▀▄ ▄▀ █  █  █   █ █ █ █ █ █ █ ▀█    █   █ █     ▀▀▀▀▄ █ ▄▀▄  ▄▀▀█ █▄▄▄█ █▄▄▄▀ █     █   █ █▄▄▄  █▄▄▄  █     █▄▄▄█   █      █  █▄▀   █     █ █ █ █▀▄ █ █   █ █▄▄▄▀ █   █ █▄▄▄▀ ▀▄▄▄    █   █   █ █   █ █ ▄ █  ▀▄▀   ▀▄▀    ▄▀   ▀▀▀▄ █▄▄▄  ▄▀▀▀▀  ▄▄▄█ ▄▀▀▀▄  ▄█▄  ▄▀▀▀█ █▄▄▄    ▄      █   █ ▄▀   █   █▀▄▀█ █▄▀▀▄ ▄▀▀▀▄ █▀▀▀▄ ▄▀▀▀█ █▄▀▀▄ ▄▀▀▀▀ ▀▀█▀▀ █   █ █   █ █   █ ▀▄ ▄▀ ▀▄ ▄▀ ▀▀▀█▀    ▀    █      ▄▀   ▀▄  ▄▀ █  ▀▀▀▀▄ █▄▄▄    ▄▀  ▀▄▄▄▀ ▀▄▄▄█ █ ▄▀█ ▄▄▄▄▄ ▀▀▀▀▀ ▀       █    ▄▄ █ ▀█▀█▀ ▀▄█▄    ▄▀  ▀   ▀ ▀▄▀   ▀▄█▄▀  █       █        ▄▄█▄▄   █     █    ▀▄    ▀▀    ▀                  ▄▀  ▄▀       ▀▄   █    ▀▀    ▀ ▀  ▄▀       ▀▄    ▄▀       ",
//    "█   █ █   █ █   █ █      █ █  █     █     █ █ █ ▄   █ █▀  █ █▀  █ █ ▀▄   █  █ █   █ █   █ █   █ █   █ █   █ █     █   ▄   █   ▄   █   █     █   ▄▀ ▀▄ █  █      █ █ █ █ █ █ █  █  █ █ █ █ █   █     █ █ █ █  ▄▀ █ ▄▀▀▀█ █   █ █▀▀▀▄ █      █ █  █▀▀▀  █▀▀▀  ▄▀█▀▄  ▀▀▀▄ █▄▀ █ █▄▀ █ █▀▀▄   █  █ █ ▀ █ █▀▀▀█ █   █ █▀▀▀█ █   █ █▀▀▀  █       █    ▀▀▀█  ▀▄▀  ▀▄█▄▀  ▄▀▄  █  █   ▀▀▀█ █ █ █ █ █ █  █▀▀▄ █▀▄ █ █▀▀▀▄  ▀▀▀█ █▀█ █  ▄▀▀█ █   █ █   █ █   ▄ █   █ █     █     █  ▀█ █   █   █   ▄  █  █ ▀▄  █     █   █ █  ▀█ █   █ █     █ ▀▄▀ █ ▀▄      █   █   █   █  █ █  █ █ █ ▄▀ ▀▄   █   ▄▀    ▄▀▀▀█ █   █ █     █   █ █▀▀▀▀   █    ▀▀▀█ █   █   █   ▄  █   █▀▄    █   █ █ █ █   █ █   █ █▀▀▀   ▀▀▀█ █      ▀▀▀▄   █ ▄ █   █ ▀▄ ▄▀ █ █ █  ▄▀▄    █    ▄▀           █    ▄▀   ▄   █ ▀▀▀█▀ ▄   █ █   █  █    █   █    ▄▀ █▀  █       ▀▀▀▀▀             █ █ █ ▀█▀█▀ ▄▄█▄▀ ▄▀ ▄▄       █ ▀▄▀ ▀ █ ▀  ▀▄     ▄▀          █     █     █      ▀▄  ▀█          ▀█    ▄▄   ▄▀     █       █    █    ██          ▀▄     ▄▀    ▀         ",
//    "▀   ▀ ▀▀▀▀  ▀▀▀▀  ▀     █▀▀▀█ ▀▀▀▀▀ ▀▀▀▀▀ ▀ ▀ ▀  ▀▀▀  ▀   ▀ ▀   ▀ ▀   ▀ ▀   ▀ ▀   ▀ ▀   ▀  ▀▀▀   ▀▀▀  ▀   ▀ ▀      ▀▀▀    ▀    ▀▀▀    ▀     ▀   ▀   ▀ ▀▀▀▀█     ▀ ▀▀▀▀▀ ▀▀▀▀█  ▀▀▀  ▀▀  ▀ ▀▀▀▀  ▀▀▀▀  ▀  ▀  ▀   ▀  ▀▀▀▀  ▀▀▀  ▀▀▀▀  ▀     █▀▀▀█  ▀▀▀▀  ▀▀▀▀ ▀ ▀ ▀ ▀▀▀▀  ▀   ▀ ▀   ▀ ▀   ▀ ▀   ▀ ▀   ▀ ▀   ▀  ▀▀▀   ▀▀▀  ▀   ▀ ▀      ▀▀▀▀   ▀    ▀▀▀    ▀     ▀   ▀   ▀ ▀▀▀▀█     ▀ ▀▀▀▀▀ ▀▀▀▀█  ▀▀▀  ▀▀  ▀ ▀▀▀▀  ▀▀▀▀  ▀  ▀  ▀   ▀ ▀   ▀ ▀▀▀▀   ▀▀▀  ▀▀▀▀  ▀▀▀▀▀ ▀      ▀▀▀  ▀   ▀  ▀▀▀   ▀▀   ▀   ▀ ▀▀▀▀▀ ▀   ▀ ▀   ▀  ▀▀▀  ▀      ▀▀ ▀ ▀   ▀ ▀▀▀▀    ▀    ▀▀▀    ▀    ▀ ▀  ▀   ▀   ▀   ▀▀▀▀▀  ▀▀▀▀ ▀▀▀▀   ▀▀▀▀  ▀▀▀▀  ▀▀▀▀   ▀   ▀▀▀▀  ▀   ▀   ▀    ▀▀    ▀  ▀  ▀▀▀  ▀   ▀ ▀   ▀  ▀▀▀  ▀         ▀ ▀     ▀▀▀▀     ▀   ▀▀▀    ▀    ▀ ▀  ▀   ▀  ▀    ▀▀▀▀▀        ▀▀▀  ▀▀▀▀▀  ▀▀▀     ▀   ▀▀▀   ▀▀▀   ▀     ▀▀▀   ▀▀    ▀▀▀                      ▀    ▀▀▀   ▀ ▀    ▀      ▀▀        ▀▀ ▀          ▀   ▀    ▀▀▀▀▀         ▀▀   ▀▀          ▀           ▀     ▀▀           ▀▀   ▀▀     ▀                  ▀   ▀      ▀         ",
//};
//static string charDataStr = "АБВГДЕЁЖЗИЙКЛМНОӨПРСТУҮФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмноөпрстуүфхцчшщъыьэюяABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz`1234567890-=~!@#$%^&*()_+[]\\;',./{}|:\"<>? ";
//public static string GetComment(string str, int tabSize = 4) {
//    string res = "";
//    string[] arr = str.Split('\n');
//    int startIdx = str.Contains("Ё") || str.Contains("Й") ? 0 : 1;
//    foreach (string s in arr) {
//        for (int i = startIdx; i < charDataArr.Length; i++) {
//            res += "\n\t// ";
//            for (int j = 0, k = 0; j < s.Length; j++) {
//                if (s[j] == '\t') {
//                    for (int it = 0, t = tabSize - k % tabSize; it < t; it++) {
//                        res += charDataArr[i].Sub(charDataStr.IndexOf(' ') * 6, 6);
//                        k++;
//                    }
//                } else if (charDataStr.Contains("" + s[j])) {
//                    res += charDataArr[i].Sub(charDataStr.IndexOf(s[j]) * 6, 6);
//                    k++;
//                }
//            }
//        }
//    }
//    return res;
//}