using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using Orgil;

public class Mb : MonoBehaviour {
    public static T[] Arr<T>(params T[] arr) => arr;
    public static List<T> Lis<T>(params T[] arr) => arr.ToList();
    public static bool IsMenu => Gc.State == GameState.Menu;
    public static bool IsGame => Gc.State == GameState.Game;
    public static bool IsComplete => Gc.State == GameState.Complete;
    public static bool IsFail => Gc.State == GameState.Fail;
    public static bool IsSettings => Gc.State == GameState.Settings;
    public static bool IsMb => Input.GetMouseButton(0);
    public static bool IsMbD => Input.GetMouseButtonDown(0);
    public static bool IsMbU => Input.GetMouseButtonUp(0);
    public static Vector3 Mp => Input.mousePosition;
    [HideInInspector]
    public Vector3 mp;
    public static Gc GC => Gc._;
    public static Cc CC => Cc._;
    public static Cm CM => Cm._;
    public static Camera Cam => Camera.main;
    public static Player P => Player._;
    public static float Dt => Time.deltaTime;
    public static float FixDt => Time.fixedDeltaTime;
    public static float SmtDt => Time.smoothDeltaTime;
    public static Vector3 G { get => Physics.gravity; set => Physics.gravity = value; }
    public GameObject go => gameObject;
    public Transform tf => transform;
    public Rigidbody rb { get { if (!_rb) _rb = gameObject.GetComponent<Rigidbody>(); return _rb; } }
    Rigidbody _rb;
    public Rigidbody2D rb2 { get { if (!_rb2) _rb2 = gameObject.GetComponent<Rigidbody2D>(); return _rb2; } }
    Rigidbody2D _rb2;
    public Transform Par { get => transform.parent; set => transform.parent = value; }
    public GameObject ParGo { get => transform.parent.gameObject; set => transform.parent = value.transform; }
    public Vector3 Tp { get => transform.position; set => transform.position = value; }
    public Vector3 Tlp { get => transform.localPosition; set => transform.localPosition = value; }
    public Quaternion Tr { get => transform.rotation; set => transform.rotation = value; }
    public Quaternion Tlr { get => transform.localRotation; set => transform.localRotation = value; }
    public Vector3 Ts => transform.lossyScale;
    public Vector3 Tls { get => transform.localScale; set => transform.localScale = value; }
    public Vector3 Te { get => transform.eulerAngles; set => transform.eulerAngles = value; }
    public Vector3 Tle { get => transform.localEulerAngles; set => transform.localEulerAngles = value; }
    public Vector3 TfL { get => -transform.right; set => transform.right = -value; }
    public Vector3 TfR { get => transform.right; set => transform.right = value; }
    public Vector3 TfD { get => -transform.up; set => transform.up = -value; }
    public Vector3 TfU { get => transform.up; set => transform.up = value; }
    public Vector3 TfB { get => -transform.forward; set => transform.forward = -value; }
    public Vector3 TfF { get => transform.forward; set => transform.forward = value; }
    public static T Ins<T>(T pf) where T : Object => Instantiate(pf);
    public static T Ins<T>(T pf, Vector3 pos, Quaternion rot) where T : Object => Instantiate(pf, pos, rot);
    public static T Ins<T>(T pf, Vector3 pos, Quaternion rot, Transform par) where T : Object => Instantiate(pf, pos, rot, par);
    public static T Ins<T>(T pf, Transform par) where T : Object => Instantiate(pf, par);
    public static T Ins<T>(T pf, Transform par, bool isWorldPosStays) where T : Object => Instantiate(pf, par, isWorldPosStays);
    public static void Dst(Object obj) => Destroy(obj);
    public static void Dst(Object obj, float t = 0f) => Destroy(obj, t);
    public void CxlIvk(string name) => CancelInvoke(name);
    public void CxlIvk() => CancelInvoke();
    public void Ivk(string name, float t) => Invoke(name, t);
    public void IvkRep(string name, float t, float rate) => InvokeRepeating(name, t, rate);
    public bool IsIvk(string name) => IsInvoking(name);
    public bool IsIvk() => IsInvoking();
    public Coroutine StaCor(string name) => StartCoroutine(name);
    public Coroutine StaCor(IEnumerator routine) => StartCoroutine(routine);
    public Coroutine StaCor(string name, object val = null) => StartCoroutine(name, val);
    public void StopCor() => StopAllCoroutines();
    public void StopCor(IEnumerator routine) => StopCoroutine(routine);
    public void StopCor(Coroutine routine) => StopCoroutine(routine);
    public void StopCor(string name) => StopCoroutine(name);
    public IEnumerator SclCor(GameObject go, Vector3 s1, Vector3 s2, float tm, EaseTp tp = EaseTp.Linear) {
        yield return Cor(t => go.Tls(V3.Lerp(s1, s2, t)), tm, tp);
    }
    public IEnumerator PosSclCor(GameObject go, Vector3 p1, Vector3 p2, Vector3 s1, Vector3 s2, float tm, EaseTp tp = EaseTp.Linear) {
        yield return Cor(t => {
            go.Tp(V3.Lerp(p1, p2, t));
            go.Tls(V3.Lerp(s1, s2, t));
        }, tm, tp);
    }
    public IEnumerator ImgColCor(Image img, Color c1, Color c2, float tm, EaseTp tp = EaseTp.Linear) {
        yield return Cor(t => img.color = C.Lerp(c1, c2, t), tm, tp);
    }
    public IEnumerator Cor(System.Action<float> act, float tm, EaseTp tp = EaseTp.Linear) {
        for (float t = 0; t < tm; t += Dt) {
            act(M.Lerp(t / tm, tp));
            yield return null;
        }
        act(1);
    }
    public IEnumerator Cors(params IEnumerator[] cors) {
        foreach (var cor in cors)
            yield return cor;
    }
    public IEnumerator Cor2(System.Action<float> act, float tm, float x = .5f, float y0 = 0, float y1 = 1, float y2 = 0, EaseTp tp1 = EaseTp.OutSine, EaseTp tp2 = EaseTp.InSine) {
        yield return Cor(t => act(t < x ? M.Lerp(y0, y1, t / x, tp1) : M.Lerp(y1, y2, (t - x) / (1 - x), tp2)), tm);
    }
}