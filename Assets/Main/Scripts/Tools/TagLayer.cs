namespace Orgil {
	public enum Tag { Untagged, Respawn, Finish, EditorOnly, MainCamera, Player, GameController, Tail, Button }
	public class Lyr { public const int Default = 0, TransparentFX = 1, IgnoreRaycast = 2, House = 3, Water = 4, UI = 5, Tail = 6, Animal = 7; }
	public class Lm { public const int Default = 1, TransparentFX = 2, IgnoreRaycast = 4, House = 8, Water = 16, UI = 32, Tail = 64, Animal = 128; }
}