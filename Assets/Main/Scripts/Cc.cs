﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class Cc : Singleton<Cc> { // Canvas Controller
    public GameObject animalPf;
    public Font font;
    public List<Text> lvlTxts;
    public List<RectTransform> menuBtns;
    public GameObject menuGo, gameGo, completeGo, complete2Go, failGo, reviveGo, settingsGo, earningsGo, noInternetGo, gameSettings,
        envGo, rateGo, earnedGo, claimGo, nextGo, totalRankGo, playGo, lvlPntGo, tabGo;
    public Image lvlImg;
    public Color rateStaCol, rateEndCol, toggleCorrectCol, toggleWrongCol;
    public Text coinTxt, earningsTxt, riseTxt;
    public List<Sprite> envSprs;
    public List<PlayerPrefsData> gameSettingsDatas;
    public List<BtnData> btnDatas;
    public List<Col> btnCols, tabCols;
    public Vector3 tabSz;
    public bool isIO;
    [DrawIf(nameof(isIO), true)] public InputField playerInp;
    [DrawIf(nameof(isIO), true)] public Text timeTxt, killTxt;
    [DrawIf(nameof(isIO), true)] public GameObject lbGame, lbEnd;
    [DrawIf(nameof(isIO), true)] public Color lbBc, lbPlayerBc;
    [DrawIf(nameof(isIO), true)] public bool isLbGameDef;
    public bool isSkin2;
    public GameObject skinModelGo, skin1Go, skin2Go;
    public List<SkinData> skins;
    public List<Skin2Data> skins2;
    public List<Col> skin2Cols;
    public List<Color> skin2TabCols;
    bool IsMenuBtnClk => menuBtns.Find(rt => rt.ActH() && rt.IsRt(Mp));
    [HideInInspector] public List<Character> chars;
    Rect lvlRect;
    Pool riseTxtPool;
    Vector3 riseA;
    Image soundImg, hapticImg;
    bool isClk = false, isAds = false, isClaim = true, isRevive = false, isGetRnd = false, isFreeCoin = false;
    GameObject coinPf, claimPntGo;
    Text claimBalanceTxt, claimCoinTxt, claimSkipTxt;
    List<int> claimBalance = Lis(2, 3, 4, 5, 4, 3, 2);
    int claimIdx, earned, coin = 0;
    float claimX, claimTm = 0, toggleX, settingW = Screen.width * 0.1f;
    VarTime<int> setting = new VarTime<int>(0);
    [ContextMenu("Font")]
    public void Font() { O.FOsOTA<Text>().ForEach(x => x.font = font); }
    void SortCharacters() { chars.Sort((a, b) => a.IsLive ? (b.IsLive ? b.lb.scr - a.lb.scr : -1) : (b.IsLive ? 1 : b.lb.scr - a.lb.scr)); }
    public void Menu() {
        Ga.Start();
        claimPntGo = claimGo.ChildGo(1, 1);
        claimBalanceTxt = claimGo.Child(2, 2).Txt();
        claimCoinTxt = claimGo.Child(2, 4).Txt();
        claimSkipTxt = claimGo.Child(3, 0).Txt();
        claimX = -claimGo.Child(1, 0, 0).Tlp().x;
        soundImg = settingsGo.Child(0, 0, 1, 1, 0).Img();
        hapticImg = settingsGo.Child(0, 0, 2, 1, 0).Img();
        toggleX = soundImg.SibNxt().Tlp().x;
        chars = O.FOsOT<Character>();
        HWP._.Init(chars);
        lvlRect = lvlImg.Rt().rect;
        riseA = V3.V(riseTxt.color.a, riseTxt.Is<Shadow>() ? riseTxt.Shadow().effectColor.a : 0, riseTxt.Is<Outline>() ? riseTxt.Outline().effectColor.a : 0);
        riseTxtPool = new Pool(riseTxt.gameObject, 30, riseTxt.Par(), true);
        lvlTxts.ForEach(x => x.text = "LEVEL " + Gc.Level);
        coinPf = coinTxt.SibNxtGo();
        Gc.Coin = Gc.Coin;
        menuGo.Show();
        gameGo.Hide();
        failGo.Hide();
        completeGo.Hide();
        complete2Go.Hide();
        gameSettings.Hide();
        Gc.State = GameState.Menu;
        StaCor(EnvCor());
        BtnInit();
        IoInit();
        SkinInit();
        // EarningsShow();
        StaCor(AnimalCor());
    }
    IEnumerator AnimalCor(int n = 8, int sta = 4, int end = 3, int cnt = 10) {
        float r = -animalPf.Tlp().x, ang = 360f / n / cnt, spc = 2 * M.PI * r / n / cnt;
        int i, j;
        List<(Vector3, Quaternion)> tfs = new List<(Vector3, Quaternion)>();
        for (i = 0; i < sta; i++)
            for (j = 0; j < cnt; j++)
                tfs.Add((V3.Xy(-r, ((i - sta) * cnt + j) * spc), Q.O));
        for (i = 0; i < n; i++)
            for (j = 0; j < cnt; j++)
                tfs.Add((Ang.V2(180 - (i * cnt + j) * ang, r), Q.Z(-(i * cnt + j) * ang)));
        for (i = 0; i < end; i++)
            for (j = 0; j < cnt; j++)
                tfs.Add((V3.Xy(-r, (i * cnt + j) * spc), Q.O));
        List<(GameObject, int)> l = Lis((animalPf, 0));
        int lCnt = sta + n + end, tfCnt = tfs.Count - 1;
        for (i = 1; i < lCnt; i++)
            l.Add((Ins(animalPf, tfs[0].Item1, tfs[0].Item2, animalPf.Par()), -i * cnt));
        List<(GameObject, Vector3)> l2 = new List<(GameObject, Vector3)>();
        for (i = 1; i <= 3; i++) {
            var cGo = animalPf.SibGo(i);
            l2.Add((cGo, cGo.Tlp()));
        }
        StaCor(AnimalIdleCor(l2));
        while (IsMenu) {
            for (i = 0; i < lCnt; i++) {
                if (l[i].Item2 >= 0) {
                    l[i].Item1.Tlp(tfs[l[i].Item2].Item1);
                    l[i].Item1.Tlr(tfs[l[i].Item2].Item2);
                }
                l[i] = (l[i].Item1, l[i].Item2 == tfCnt ? 0 : l[i].Item2 + 1);
            }
            yield return Wf.Sec(.025f);
        }
    }
    IEnumerator AnimalIdleCor(List<(GameObject, Vector3)> l) {
        int i, lCnt = l.Count;
        while (IsMenu) {
            yield return Cor2(t => {
                for (i = 0; i < lCnt; i++) {
                    l[i].Item1.Tlp(l[i].Item2 + V3.Y(t * 10));
                }
            }, .3f);
        }
    }
    void Update() {
        if (isAds && !isClaim) {
            claimPntGo.TlpX(M.Lerp(-claimX, claimX, M.Sin01(claimTm * 180)));
            claimIdx = M.RoundI(claimPntGo.Tlp().x / claimX * 3 + 3);
            claimBalanceTxt.text = claimBalance[claimIdx] + "X";
            claimCoinTxt.text = "+" + (earned * claimBalance[claimIdx]);
            claimTm += Dt;
        }
        if (skin2Go.activeSelf && skin2Tabs.IsCount()) {
            if (IsMbD) {
                isClk = true;
                mp = Mp;
            }
            if (IsMb) {
                skinsParGo.TpDx(Mp.x - mp.x);
                mp = Mp;
            }
            if (IsMbU) {
                isClk = false;
                List<float> xs = new List<float>();
                for (int i = 0; i < skins2.Count; i++)
                    xs.Add(skinsParGo.Child(i).Tp().x);
                skin2Idx = xs.NearIdx(Screen.width / 2);
                Skin2Tabs();
            }
            if (!isClk) {
                float x = skinsParGo.Child(skin2Idx).Tp().x;
                skinsParGo.TpDx(M.Lerp(x, Screen.width / 2, Dt * 10) - x);
            }
        }
        if (isIO && IsGame) {
            Gc.Tm -= Dt;
            if (!P.IsLive) {
                P.Done();
                Fail();
            } else if (Gc.Tm <= 0) {
                SortCharacters();
                P.Done();
                if (chars[0].IsPlayer) Complete();
                else Fail();
            }
            IoData();
        }
        if (IsMbD) {
            if (Mp.x < settingW && Mp.y > Screen.height - settingW) {
                setting.Set(this, nameof(setting), 0.5f, setting.Val + 1, 0);
                if (setting.Val % 2 == 0)
                    riseTxt.SibPrvGo().ActRev();
                if (setting.Val == 10)
                    Gc.Settings = !Gc.Settings;
            }
            if (Application.isEditor || Gc.Settings)
                if (Mp.x > Screen.width - settingW && Mp.y > Screen.height - settingW) {
                    GameSettings();
                } else if (Mp.x < settingW && Mp.y < settingW && Gc.Level > 1) {
                    Gc.Level--;
                    Replay();
                } else if (Mp.x > Screen.width - settingW && Mp.y < settingW) {
                    Gc.Level++;
                    Replay();
                }
        }
        if (IsMenu && !playGo.activeSelf && IsMbD && !IsMenuBtnClk)
            Play();
    }
    public void Play() {
        if (isIO) {
            Data.Player.S(playerInp.text);
            P.lb.name = Data.Player.S();
        }
        menuGo.Hide();
        gameGo.Show();
        Level(0);
        Gc.State = GameState.Game;
        P.MbD();
    }
    public void Fail() {
        if (isIO && P.IsLive)
            lbEnd.Show();
        Ga.Fail();
        gameGo.Hide();
        failGo.Show();
        Gc.State = GameState.Fail;
        Gc.IsWin = false;
        if (Gc.Best < Gc.Score)
            Gc.Best = Gc.Score;
    }
    public void Complete(float rate = 0, int dCoin = 0) {
        Ga.Complete();
        gameGo.Hide();
        completeGo.Show();
        // StaCor(RateCor(rate, 0.5f));
        // earnedGo.Act(!isAds);
        // claimGo.Act(isAds);
        // nextGo.Act(!isAds);
        // if (isAds) {
        //     isClaim = false;
        //     earned = dCoin;
        //     claimSkipTxt.text = "GET " + earned;
        // } else {
        //     earnedGo.Child(1).Txt().text = "" + dCoin;
        //     if (dCoin > 0)
        //         CrtCoinAnim(earnedGo.Child(2).Tp(), dCoin);
        // }
        Gc.State = GameState.Complete;
        Gc.IsWin = true;
        Gc.Level++;
        if (isIO) {
            lbEnd.Show();
            IoData();
        }
    }
    public void Level(float f) {
        lvlImg.fillAmount = f;
        if (lvlPntGo) lvlPntGo.Progress(f, lvlRect);
    }
    public void SettingsShow() {
        settingsGo.Show();
        Toggle(hapticImg, Gc.IsHaptic);
        Toggle(soundImg, Gc.IsSound);
    }
    public void SettingsHide() { settingsGo.Hide(); }
    public void Haptic() { Toggle(hapticImg, Gc.IsHaptic = !Gc.IsHaptic); }
    public void Sound() { Toggle(soundImg, Gc.IsSound = !Gc.IsSound); }
    void Toggle(Image img, bool isCheck) {
        img.color = isCheck ? toggleCorrectCol : toggleWrongCol;
        img.SibNxt().TlpX(isCheck ? toggleX : -toggleX);
    }
    public void Replay() {
        if (coin > 0)
            Gc.Coin = coin;
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
    IEnumerator EnvCor(int cnt = 5, float spc = 100) {
        if (envGo.activeSelf) {
            int idx = (Gc.Level - 1) % cnt, envIdx = (Gc.Level - 1) / cnt;
            bool isLast = idx == 0 && Gc.Level > 1;
            envGo.Rt().SzX(cnt * spc);
            envGo.ChildGo(2, 0, 0).Img().sprite = envSprs.RepIdx(envIdx);
            envGo.ChildGo(3, 0, 0).Img().sprite = envSprs.RepIdx(envIdx + 1);
            GameObject pf = envGo.ChildGo(1, 0);
            for (int i = 0; i < cnt; i++) {
                int j = isLast ? 0 : (i < idx ? 0 : i == idx ? 1 : 2);
                GameObject lvlGo = i == 0 ? pf : Ins(pf, pf.Par());
                lvlGo.TlpX(i.X(cnt, 0, spc));
                lvlGo.ChildShow(j);
                if (j > 0) lvlGo.Child(j, 0, 0).Txt().text = "" + (envIdx * cnt + i + 1);
            }
            Image img = envGo.ChildGo(0, 0).Img();
            Vector2 p = isLast ? V2.V(1 - 0.5f / cnt, 1) : V2.V(M.C01((idx - 0.5f) / cnt), (idx + 0.5f) / cnt);
            GameObject pnt = envGo.ChildGo(0, 1);
            yield return Cor(t => pnt.Progress(img.fillAmount = M.Lerp(p.x, p.y, t), img.Rt().rect), 0.3f);
            if (isLast) yield return SclCor(pnt, V3.I, V2.O, 0.2f);
        }
    }
    public void Revive() { StaCor(ReviveCor()); }
    IEnumerator ReviveCor(float tm = 3) {
        if (isRevive || !isAds) {
            Fail();
        } else {
            reviveGo.Show();
            Image reviveImg = reviveGo.Child(1, 0).Img();
            Text reviveTxt = reviveGo.Child(1, 1).Txt();
            for (float t = 0; t < tm && !isRevive; t += Dt) {
                reviveImg.fillAmount = 1 - t / tm;
                reviveTxt.text = "" + M.CeilI(tm - t);
                yield return null;
            }
            if (!isRevive) {
                reviveGo.Hide();
                Fail();
            }
        }
    }
    public void Revive(bool isSkip) {
        reviveGo.Hide();
        if (isSkip) {
            Fail();
        } else {
            isRevive = true;
            P.Revive();
        }
    }
    IEnumerator RateCor(float rate, float tm) {
        Text txt = rateGo.Child(0).Txt();
        Image img = rateGo.Child(1, 0, 0, 0).Img();
        Rect rect = img.Rt().rect;
        List<float> l = Lis(0.5f, 0.75f, 0.9f);
        List<GameObject> gos = new List<GameObject>();
        for (int i = 0; i < l.Count; i++) {
            gos.Add(rateGo.ChildGo(1, i + 1, 0));
            gos[i].ParGo().Progress(l[i], rect);
            gos[i].Tls(V3.O);
        }
        int j = 0;
        yield return Cor(t => {
            img.fillAmount = t * rate;
            img.color = C.Lerp(rateStaCol, rateEndCol, M.Lerp(img.fillAmount, EaseTp.OutQuart));
            txt.text = "RATE: " + M.RoundI(img.fillAmount * 100) + "%";
            if (j < l.Count && img.fillAmount >= l[j])
                StaCor(SclCor(gos[j++], V3.O, V3.I, 0.2f, EaseTp.OutSine));
        }, tm);
    }
    public void Claim(bool isSkip) { StaCor(ClaimCor(isSkip)); }
    IEnumerator ClaimCor(bool isSkip) {
        if (!isClaim) {
            isClaim = true;
            if (isSkip) {
                Gc.Coin = Gc.Coin + earned;
            } else {
                CrtCoinAnim(claimCoinTxt.SibLast().Tp(), earned * claimBalance[claimIdx]);
                Text txt = claimGo.Child(1, 0, claimIdx, 0).Txt();
                txt.Par(txt.Par(2));
                yield return SclCor(txt.gameObject, V3.I, V3.V(2), 0.3f);
                yield return Cor(t => txt.ColA(1 - t), 0.2f);
                yield return Wf.Sec(1);
            }
            completeGo.Hide();
            complete2Go.Show();
            yield return TotalRankCor();
        }
    }
    float CrtCoinAnim(Vector3 p, int dCoin) {
        coin = Gc.Coin + dCoin;
        return CrtAnim(coinPf, p, coinPf.Tp(), Gc.Coin, dCoin, c => Gc.Coin = c, 15, Screen.width / 12, Screen.width / 4);
    }
    IEnumerator TotalRankCor(float waitTm = 0.5f, float btnTm = 0.5f, float upTm = 5, float downTm = 0.5f) {
        totalRankGo.Show();
        GameObject placesGo = totalRankGo.ChildGo(0, 0), placePf = placesGo.ChildGo(0), playerGo = placesGo.ChildGo(1);
        int endPlc = Rnd.RngIn(20000, 25000), staCnt = 3, rowCnt = 7, cnt = 50, playerStaPlc = endPlc - staCnt + 1, playerEndPlc = playerStaPlc - cnt + rowCnt, staScr = Rnd.RngIn(1500, 2000), endScr = staScr + Rnd.RngIn(50, 200);
        float h = placePf.Rt().rect.height, spc = 15, bor = 20;
        totalRankGo.Rt().SzY(rowCnt * h + (rowCnt + 1) * spc + bor * 2);
        System.Func<int, Vector3> pos = i => V3.Y(spc + h / 2 + i * (spc + h));
        List<GameObject> places = new List<GameObject>();
        for (int i = 0; i < cnt; i++) {
            GameObject placeGo = i == 0 ? placePf : Ins(placePf, placePf.Par());
            placeGo.Tlp(pos(i));
            placeGo.Child(0).Txt().text = "#" + (endPlc - i);
            placeGo.Child(1).Img().sprite = O.Load<Sprite>("Flags/flag" + Rnd.RngIn(1, 263));
            placeGo.Child(2).Txt().text = O.Names.Rnd();
            places.Add(placeGo);
        }
        playerGo.Tlp(pos(staCnt - 1));
        playerGo.Child(0).Txt().text = "#" + playerStaPlc;
        playerGo.Child(1).Inp().text = Data.Player.S();
        playerGo.Child(2).Txt().text = "" + staScr;
        playerGo.Par(playerGo.Par(2));
        yield return Wf.Sec(waitTm);
        yield return SclCor(playerGo, V3.I, V3.V(1.1f), btnTm, EaseTp.OutSine);
        Vector3 staP = placesGo.Tlp(), endP = staP - V3.Y((spc + h) * (cnt - rowCnt));
        yield return Cor(t => {
            placesGo.Tlp(V3.Lerp(staP, endP, t));
            playerGo.Child(0).Txt().text = "#" + M.RemapI(t, 0, 1, playerStaPlc, playerEndPlc);
            playerGo.Child(2).Txt().text = "" + M.RemapI(t, 0, 1, staScr, endScr);
        }, upTm, EaseTp.OutCirc);
        yield return SclCor(playerGo, V3.V(1.1f), V3.I, btnTm, EaseTp.InSine);
        for (int i = 0; i < staCnt; i++) {
            GameObject cGo = places[cnt - rowCnt + i];
            Vector3 staP2 = cGo.Tlp(), endP2 = staP2 - V3.Y(spc + h);
            StaCor(Cor(t => cGo.Tlp(V3.Lerp(staP2, endP2, t)), downTm));
        }
        yield return Wf.Sec(downTm);
        for (int i = 0; i < staCnt; i++)
            places[cnt - rowCnt + i].Child(0).Txt().text = "#" + (endPlc - cnt + rowCnt - i + 1);
    }
    public void CoinAnim(Vector3 p, int coin) { CrtAnim(coinPf, p, coinPf.Tp(), () => Gc.Coin += coin); }
    float CrtAnim(GameObject pf, Vector3 staP, Vector3 endP, int sta, int add, Action<int> act, int cnt, float minR, float maxR) {
        List<Vector2> l = new List<Vector2>();
        for (int i = 0; i < cnt; i++)
            l.Add(V2.V(Rnd.Rng(0.3f, 0.5f), Rnd.Rng(0.3f, 0.5f)));
        l.Sort((a, b) => (a.x + a.y).CompareTo(b.x + b.y));
        for (int i = 0; i < cnt; i++) {
            int j = M.RoundI(sta + (i + 1f) / cnt * add);
            StaCor(CrtAnimCor(pf, staP, () => act(j),
                (staP + Ang.V2(i.Ang(cnt), Rnd.Rng(minR, maxR)).V3(), l[i].x, EaseTp.Linear),
                (endP, l[i].y, EaseTp.InCubic)));
        }
        return l.Last().x + l.Last().y;
    }
    void CrtAnim(GameObject pf, Vector3 p, Vector3 endP, Action act) {
        StaCor(CrtAnimCor(pf, Cam.WorldToScreenPoint(p), act, (endP, 1, EaseTp.InCubic)));
    }
    IEnumerator CrtAnimCor(GameObject pf, Vector3 p, Action endAct, params (Vector3, float, EaseTp)[] arr) {
        var l = Lis(arr);
        l.Insert(0, (p, 0, EaseTp.Linear));
        GameObject go = Ins(pf, p, Q.O, tf);
        for (int i = 1; i < l.Count; i++)
            for (float t = 0; t < l[i].Item2; t += SmtDt) {
                go.Tp(V3.Lerp(l[i - 1].Item1, l[i].Item1, M.Lerp(0, 1, t / l[i].Item2, l[i].Item3)));
                yield return null;
            }
        endAct();
        Dst(go);
    }
    public void RiseTxt(bool isKill, Vector3 p, string str, float h = 0.3f, float tm = 0.6f, float th = 0.6f) { StaCor(RiseTxtCor(isKill, p, str, h * Screen.height, tm, th)); }
    IEnumerator RiseTxtCor(bool isKill, Vector3 p, string str, float h, float tm, float th) {
        Vector2 staP = Cam.WorldToScreenPoint(p), endP = staP + V2.Y(h);
        Text txt = riseTxtPool.Get(staP).Txt();
        Shadow shadow = txt.Shadow();
        Outline outline = txt.Outline();
        txt.text = str;
        txt.fontSize = isKill ? 80 : 60;
        txt.color = isKill ? P.lb.col : C.I;
        if (shadow) shadow.enabled = !isKill;
        if (outline) outline.enabled = !isKill;
        txt.ColA(riseA.x);
        if (shadow) shadow.ColA(riseA.y);
        if (outline) outline.ColA(riseA.z);
        for (float t = 0; t < tm; t += SmtDt) {
            txt.Tp(V3.Lerp(staP, endP, t / tm));
            if (t / tm >= th) {
                float a = M.Lerp(1, 0, (t / tm - th) / (1 - th));
                txt.ColA(riseA.x * a);
                if (shadow) shadow.ColA(riseA.y * a);
                if (outline) outline.ColA(riseA.z * a);
            }
            yield return null;
        }
        txt.Hide();
    }
    List<Btn> btns = new List<Btn>();
    void BtnInit() {
        for (int i = 0; i < btnDatas.Count; i++) {
            int j = i;
            btnDatas[i].btn.onClick.AddListener(() => BtnUpd(j));
            btns.Add(new Btn(btnDatas[i]));
            menuBtns.Add(btnDatas[i].btn.Rt());
        }
        BtnUpd();
    }
    int BtnPrice(int idx, int lvl) { return (lvl + 1) * 100; }
    public void BtnUpd(int idx = -1) {
        if (Data.BtnLvls.S() == "")
            Data.BtnLvls.ListI(0.ListN(btns.Count));
        List<int> lvls = Data.BtnLvls.ListI();
        if (M.IsCnt(idx, btns.Count)) {
            int price = BtnPrice(idx, lvls[idx]);
            if (price <= Gc.Coin && lvls[idx] < btns[idx].maxLvl) {
                Gc.Coin -= price;
                lvls[idx]++;
                Data.BtnLvls.ListI(lvls);
            }
        }
        for (int i = 0; i < btns.Count; i++) {
            int price = BtnPrice(i, lvls[i]);
            if (lvls[i] >= btns[i].maxLvl) btns[i].Upd(btnCols[2], true, btns[i].maxLvl, 0);
            else if (price <= Gc.Coin) btns[i].Upd(btnCols[1], false, lvls[i] + 1, price);
            else btns[i].Upd(btnCols[0], false, lvls[i] + 1, price);
        }
        // update button data
    }
    List<Tab> tabs = new List<Tab>();
    void TabInit() {
        Transform contentTf = tabGo.Child(0, 0);
        for (int i = 0, n = tabGo.Child(1).Tcc(); i < n; i++) {
            if (i > 0)
                Ins(contentTf, contentTf.parent);
            int j = i;
            tabGo.Child(1, i).Btn().onClick.AddListener(() => TabUpd(j));
            tabs.Add(new Tab(tabGo.Child(0, i, 0), tabGo.Child(1, i)));
            menuBtns.Add(tabs[i].rt);
        }
        menuBtns.Add(tabGo.Rt());
        TabUpd(0);
        // create tab data
    }
    public void TabUpd(int idx) {
        for (int i = 0; i < tabs.Count; i++) {
            if (i == idx) tabs[i].Upd(V2.V(tabSz.x * i, -10), tabSz.V2() + V2.Y(tabSz.z), tabCols[0]);
            else tabs[i].Upd(V2.V(tabSz.x * i, 0), tabSz.V2(), tabCols[1]);
            tabs[i].contentTf.ParAct(i == idx);
        }
    }
    List<Lb> lbGames = new List<Lb>();
    List<Lb2> lbEnds = new List<Lb2>();
    void IoInit() {
        if (isIO) {
            playerInp.text = Data.Player.S();
            for (int i = 0; i < lbGame.Tcc(); i++)
                lbGames.Add(new Lb(lbGame, i));
            for (int i = 0; i < lbEnd.Tcc(); i++)
                lbEnds.Add(new Lb2(lbEnd, i));
            IoData();
        }
    }
    void IoData() {
        timeTxt.text = Gc.Tm.TimeS();
        killTxt.text = "Kills: " + P.lb.kill;
        SortCharacters();
        int idx = chars.FindIndex(c => c.IsPlayer);
        if (IsMenu || IsGame) {
            for (int i = 0, n = lbGames.Count - 1; i <= n; i++) {
                if (i >= chars.Count || !chars[i].IsLive || i == n && idx < n) {
                    lbGames[i].bgImg.Act(false);
                } else {
                    int j = i == n ? idx : i;
                    var lb = chars[j].lb;
                    lbGames[i].bgImg.Act(true);
                    lbGames[i].bgImg.color = lb.isPlayer ? isLbGameDef ? lbPlayerBc : lb.col : lbBc;
                    lbGames[i].txt.text = (j + 1) + " - " + lb.scr + "pts " + lb.name;
                    lbGames[i].txt.color = !isLbGameDef && lb.isPlayer ? C.I : lb.col;
                    lbGames[i].shadow.enabled = !lb.isPlayer;
                }
            }
        } else {
            for (int i = 0, n = lbEnds.Count - 1; i <= n; i++) {
                if (i >= chars.Count) {
                    lbEnds[i].bgImg.Act(false);
                } else {
                    int j = i == n && idx > n ? idx : i;
                    var lb = chars[j].lb;
                    lbEnds[i].bgImg.Act(true);
                    lbEnds[i].bgImg.color = lb.isPlayer ? lbPlayerBc : C.O;
                    lbEnds[i].placeTxt.text = j == 0 ? "1st" : j == 1 ? "2nd" : j == 2 ? "3rd" : (j + 1) + "th";
                    lbEnds[i].deadGo.Act(!lb.isLive);
                    lbEnds[i].nameTxt.text = lb.scr + "pts " + lb.name;
                    lbEnds[i].nameTxt.color = lb.col;
                    lbEnds[i].killTxt.ParAct(lb.kill > 0);
                    lbEnds[i].killTxt.text = "" + lb.kill;
                    lbEnds[i].scrTxt.text = "" + lb.scr2;
                }
            }
        }
    }
    GameState tmpGs;
    public void GameSettings() {
        if (gameSettings.activeSelf) {
            gameSettings.Hide();
            Time.timeScale = 1;
            Gc.State = tmpGs;
        } else {
            gameSettings.Show();
            gameSettingsDatas.ForEach(x => ShowData(x.key, x.go));
            Time.timeScale = 0;
            tmpGs = Gc.State;
            Gc.State = GameState.Settings;
        }
    }
    public void GameSettingsClear() {
        PlayerPrefs.DeleteAll();
        gameSettingsDatas.ForEach(x => ResetData(x.key, x.go));
    }
    public void GameSettingsSave() { gameSettingsDatas.ForEach(x => SaveData(x.key, x.go)); }
    void ResetData(Data key, GameObject go) {
        var p = O.Property(GC, key.tS());
        Type type = Gc.Datas[key].GetType();
        if (go.name == "Dropdown") {
            p.SetValue(GC, go.Drp().value = key.I());
        } else if (go.name == "InputField") {
            InputField inp = go.Inp();
            if (type.IsTp<float>()) p.SetValue(GC, GetValSetInp<float>(inp, key));
            else if (type.IsTp<int>()) p.SetValue(GC, GetValSetInp<int>(inp, key));
            else if (type.IsTp<string>()) p.SetValue(GC, GetValSetInp<string>(inp, key));
        } else if (go.name == "Slider") {
            p.SetValue(GC, go.Sld().value = key.F());
        } else if (go.name == "Toggle") {
            p.SetValue(GC, go.Tgl().isOn = key.B());
        } else if (go.name == "Vector") {
            if (type.IsTp<Vector2>()) {
                Vector2 v = key.V2();
                SetVecGo(go, v.x, v.y);
                p.SetValue(GC, v);
            } else if (type.IsTp<Vector2Int>()) {
                Vector2Int v = key.V2I();
                SetVecGo(go, v.x, v.y);
                p.SetValue(GC, v);
            } else if (type.IsTp<Vector3>()) {
                Vector3 v = key.V3();
                SetVecGo(go, v.x, v.y, v.z);
                p.SetValue(GC, v);
            } else if (type.IsTp<Vector3Int>()) {
                Vector3Int v = key.V3I();
                SetVecGo(go, v.x, v.y, v.z);
                p.SetValue(GC, v);
            } else if (type.IsTp<Vector4>()) {
                Vector4 v = key.V4();
                SetVecGo(go, v.x, v.y, v.z, v.w);
                p.SetValue(GC, v);
            }
        }
    }
    void ShowData(Data key, GameObject go) {
        var p = O.Property(GC, key.tS());
        Type type = Gc.Datas[key].GetType();
        if (go.name == "Dropdown") {
            go.Drp().value = (int)p.GetValue(GC);
        } else if (go.name == "InputField") {
            InputField inp = go.Inp();
            if (type.IsTp<int>()) inp.text = "" + (int)p.GetValue(GC);
            else if (type.IsTp<float>()) inp.text = "" + (float)p.GetValue(GC);
            else if (type.IsTp<string>()) inp.text = "" + (string)p.GetValue(GC);
        } else if (go.name == "Slider") {
            go.Sld().value = (float)p.GetValue(GC);
        } else if (go.name == "Toggle") {
            go.Tgl().isOn = (bool)p.GetValue(GC);
        } else if (go.name == "Vector") {
            if (type.IsTp<Vector2>()) {
                Vector2 v = (Vector2)p.GetValue(GC);
                SetVecGo(go, v.x, v.y);
            } else if (type.IsTp<Vector2Int>()) {
                Vector2Int v = (Vector2Int)p.GetValue(GC);
                SetVecGo(go, v.x, v.y);
            } else if (type.IsTp<Vector3>()) {
                Vector3 v = (Vector3)p.GetValue(GC);
                SetVecGo(go, v.x, v.y, v.z);
            } else if (type.IsTp<Vector3Int>()) {
                Vector3Int v = (Vector3Int)p.GetValue(GC);
                SetVecGo(go, v.x, v.y, v.z);
            } else if (type.IsTp<Vector4>()) {
                Vector4 v = (Vector4)p.GetValue(GC);
                SetVecGo(go, v.x, v.y, v.z, v.w);
            }
        }
    }
    void SaveData(Data key, GameObject go) {
        var p = O.Property(GC, key.tS());
        Type type = Gc.Datas[key].GetType();
        if (go.name == "Dropdown") {
            p.SetValue(GC, go.Drp().value);
        } else if (go.name == "InputField") {
            string text = go.Inp().text;
            if (type.IsTp<int>()) p.SetValue(GC, text.I());
            else if (type.IsTp<float>()) p.SetValue(GC, text.F());
            else if (type.IsTp<string>()) p.SetValue(GC, text);
        } else if (go.name == "Slider") {
            p.SetValue(GC, go.Sld().value);
        } else if (go.name == "Toggle") {
            p.SetValue(GC, go.Tgl().isOn);
        } else if (go.name == "Vector") {
            if (type.IsTp<Vector2>()) p.SetValue(GC, new Vector2(InpVal(go, 0), InpVal(go, 1)));
            else if (type.IsTp<Vector2Int>()) p.SetValue(GC, new Vector2(InpVal(go, 0), InpVal(go, 1)).V2I());
            else if (type.IsTp<Vector3>()) p.SetValue(GC, new Vector3(InpVal(go, 0), InpVal(go, 1), InpVal(go, 2)));
            else if (type.IsTp<Vector3Int>()) p.SetValue(GC, new Vector3(InpVal(go, 0), InpVal(go, 1), InpVal(go, 2)).V3I());
            else if (type.IsTp<Vector4>()) p.SetValue(GC, new Vector4(InpVal(go, 0), InpVal(go, 1), InpVal(go, 2), InpVal(go, 3)));
        }
        key.Set(p.GetValue(GC));
    }
    void SetVecGo(GameObject go, params float[] vals) {
        for (int i = 0; i < vals.Length; i++)
            go.Child<InputField>(i).text = "" + vals[i];
    }
    T GetValSetInp<T>(InputField inp, Data key) {
        T val = key.Get<T>();
        inp.text = "" + val;
        return val;
    }
    float InpVal(GameObject go, int i) { return go.Child<InputField>(i).text.F(); }
    int totalSecs => (int)(System.DateTime.UtcNow - new System.DateTime(2020, 1, 1)).TotalSeconds;
    void EarningsShow() {
        if (Data.Time.I() > 0) {
            int tm = totalSecs - Data.Time.I();
            if (tm >= 3600) {
                earningsGo.Show();
                earned = tm / 360;
                earningsTxt.text = "" + earned;
            }
        }
    }
    public void Earnings() { StaCor(EarningsCor()); }
    IEnumerator EarningsCor() {
        if (earned > 0) {
            CrtCoinAnim(earningsTxt.SibNxt().Tp(), earned);
            earned = 0;
            yield return Wf.Sec(1);
            earningsGo.Hide();
        }
    }
    void OnApplicationQuit() { Data.Time.I(totalSecs); }
    int skinIdx, skin2Idx;
    public string SkinName => skinGos[skinIdx].name;
    List<GameObject> skinGos = new List<GameObject>();
    List<Sprite> skinSprs = new List<Sprite>();
    List<Image> skin2Tabs = new List<Image>();
    GameObject skinGo, skinPf, skinSelectGo, skinsParGo;
    Text skinNameTxt, skinPriceTxt, skinInfoTxt;
    Color skinBgCol, skinBorCol;
    void SkinInit() {
        skinGo = isSkin2 ? skin2Go : skin1Go;
        List<string> l = Data.Skin.ListS(), l2 = new List<string>();
        if (isSkin2) skins2.ForEach(v => skinSprs.Add(v.sprs));
        else skins.ForEach(v => skinSprs.Add(v.spr));
        if (l.Count != skinSprs.Count) {
            for (int i = 0; i < skinSprs.Count; i++)
                l2.Add(skinSprs[i].name, l.Contains(skinSprs[i].name) ? l[l.IndexOf(skinSprs[i].name) + 1] : i == 0 ? "2" : "0");
            Data.Skin.ListS(l = l2);
        }
        if (isSkin2) {
            skinsParGo = skinGo.ChildGo(3);
            skinPf = skinsParGo.ChildGo(0, 0);
            skinSelectGo = skinGo.ChildGo(5, 0);
            Image tabPf = skinGo.Child(4, 0).Img();
            if (skins2.Count > 1) {
                for (int i = 0; i < skins2.Count; i++)
                    skin2Tabs.Add(i == 0 ? tabPf : Ins(tabPf, tabPf.Par()));
            } else {
                tabPf.ParHide();
            }
            for (int i = 0, i1 = 0; i1 < skins2.Count; i1++)
                for (int i2 = 0; i2 < skins2[i1].sprs.Count; i2++, i++) {
                    GameObject bGo = i == 0 ? skinPf : Ins(skinPf, skinsParGo.Child(i1));
                    bGo.name = skinSprs[i].name;
                    bGo.Child(1).Img().sprite = skinSprs[i];
                    int j = i;
                    bGo.Btn().onClick.AddListener(delegate { SkinChoose(j); });
                    skinGos.Add(bGo);
                }
        } else {
            skinPf = skinGo.ChildGo(0, 0, 0, 0);
            skinNameTxt = skinGo.Child(4).Txt();
            skinPriceTxt = skinGo.Child(5, 0).Txt();
            skinSelectGo = skinGo.ChildGo(6);
            skinInfoTxt = skinGo.Child(7).Txt();
            skinBgCol = skinPf.Child(0).Img().color;
            skinBorCol = skinPf.Img().color;
            for (int i = 0; i < skins.Count; i++) {
                GameObject bGo = i == 0 ? skinPf : Ins(skinPf, skinPf.Par());
                bGo.name = skinSprs[i].name;
                bGo.Child(1).Img().sprite = skinSprs[i];
                int j = i;
                bGo.Btn().onClick.AddListener(() => SkinChoose(j));
                skinGos.Add(bGo);
            }
            RectTransform rt = skinGo.Child(0, 0, 0).Rt();
            GridLayoutGroup grid = rt.Gc<GridLayoutGroup>();
            rt.sizeDelta = rt.sizeDelta.Y((grid.cellSize.y + grid.spacing.y) * ((skins.Count - 1) / grid.constraintCount + 1));
        }
        // SkinChoose(l.IndexOf("2") / 2);
        if (isSkin2) {
            skin2Idx = skinGos[skinIdx].Par().SibIdx();
            skinsParGo.TpDx(Screen.width / 2 - skinsParGo.Child(skin2Idx).Tp().x);
            StaCor(SkinGetRndCor(false));
            Skin2Tabs();
        }
    }
    void Skin2Tabs() {
        for (int i = 0; i < skin2Tabs.Count; i++)
            skin2Tabs[i].color = i == skin2Idx ? skin2TabCols[0] : skin2TabCols[1];
    }
    public void SkinShow() { StaCor(SkinShowCor()); }
    IEnumerator SkinShowCor() {
        skinGo.Show();
        for (float t = 0; skinGo.activeInHierarchy; t += Dt) {
            skinModelGo.TleY(t * 30);
            yield return null;
        }
    }
    public void SkinHide() {
        SkinChoose(Data.Skin.ListS().IndexOf("2") / 2);
        skinGo.Hide();
        skinModelGo.TleY(0);
    }
    bool IsSkin(List<string> l, string name, int val = 0) { return l[l.IndexOf(name) + 1] == "" + val; }
    bool IsSkin(List<string> l, int i, int val = 0) { return IsSkin(l, skinGos[i].name, val); }
    public void SkinSelect() {
        List<string> l = Data.Skin.ListS();
        if (IsSkin(l, skinIdx, 1) || IsSkin(l, skinIdx) && (skins[skinIdx].price <= Gc.Coin)) {
            if (IsSkin(l, skinIdx))
                Gc.Coin -= skins[skinIdx].price;
            l[l.IndexOf("2")] = "1";
            l[l.IndexOf(SkinName) + 1] = "2";
            Data.Skin.ListS(l);
            SkinChoose(skinIdx);
        }
    }
    public void SkinGetRnd() { StaCor(SkinGetRndCor()); }
    void Skin2Upd(int i, int j) {
        skinGos[i].Img().color = skin2Cols[j].bor;
        skinGos[i].Shadow().effectColor = skin2Cols[j].shadow;
        skinGos[i].Child(0).Img().color = skin2Cols[j].bg;
        skinGos[i].Child(1).Img().color = j == 0 || j == 3 ? C._b : C.w;
    }
    IEnumerator SkinGetRndCor(bool isRnd = true, int cnt = 10, float tm = 0.1f) {
        if (!isGetRnd) {
            isGetRnd = true;
            Skin2Data s2 = skins2[skin2Idx];
            List<string> l = Data.Skin.ListS(), names = new List<string>();
            foreach (var spr in s2.sprs)
                if (IsSkin(l, spr.name))
                    names.Add(spr.name);
            if (names.IsCount() && s2.Price <= Gc.Coin) {
                if (isRnd) {
                    string nm = names.Rnd();
                    int i = l.IndexOf(nm) / 2, i0 = i;
                    if (names.Count == 1)
                        cnt = 1;
                    for (int k = 0; k < cnt; k++) {
                        if (k > 0)
                            Skin2Upd(i0, 0);
                        Skin2Upd(i, 3);
                        if (k < cnt - 1) {
                            nm = names.RndE(nm);
                            i0 = i;
                            i = l.IndexOf(nm) / 2;
                        }
                        yield return Wf.Sec(tm);
                    }
                    l[l.IndexOf("2")] = "1";
                    l[l.IndexOf(nm) + 1] = "2";
                    Data.Skin.ListS(l);
                    SkinChoose(i);
                    Gc.Coin -= s2.Price;
                    Gc.SkinCnt++;
                }
                SkinSelBtn(btnCols[s2.Price <= Gc.Coin ? (names.Count == 1 ? 2 : 1) : 0], "" + s2.Price);
            } else {
                SkinSelBtn(btnCols[s2.Price <= Gc.Coin ? (names.Count == 0 ? 2 : 1) : 0], "" + s2.Price);
            }
            isGetRnd = false;
        }
    }
    public void SkinFreeCoin() { StaCor(SkinFreeCoinCor()); }
    IEnumerator SkinFreeCoinCor() {
        if (!isFreeCoin) {
            isFreeCoin = true;
            CrtCoinAnim(skin2Go.Child(5, 1, 1).Tp(), skin2Go.Child(5, 1, 0).Txt().text.I());
            yield return Wf.Sec(1);
            Gc.Coin = coin;
            StaCor(SkinGetRndCor(false));
            isFreeCoin = false;
        }
    }
    void SkinChoose(int idx) {
        List<string> l = Data.Skin.ListS();
        if (isSkin2) {
            if (!IsSkin(l, idx)) {
                skinIdx = idx;
                l[l.IndexOf("2")] = "1";
                l[l.IndexOf(SkinName) + 1] = "2";
                Data.Skin.ListS(l);
                for (int i = 0; i < skinGos.Count; i++)
                    Skin2Upd(i, IsSkin(l, i, 2) ? 2 : IsSkin(l, i, 1) ? 1 : 0);
                SkinCrt(idx);
            }
        } else {
            skinIdx = idx;
            skinNameTxt.text = SkinName;
            skinPriceTxt.ParAct(IsSkin(l, idx));
            skinPriceTxt.text = "" + skins[idx].price;
            skinInfoTxt.text = skins[idx].info;
            int sel = IsSkin(l, idx, 2) ? 3 : IsSkin(l, idx, 1) ? 2 : skins[idx].price <= Gc.Coin ? 1 : 0;
            SkinSelBtn(btnCols[sel == 0 ? 0 : sel % 2 == 1 ? 1 : 2], sel == 3 ? "SELECTED" : "SELECT");
            for (int i = 0; i < skinGos.Count; i++) {
                skinGos[i].Img().color = i == idx ? skinBorCol : skinBgCol;
                skinGos[i].ChildAct(IsSkin(l, i) && i != idx, 2);
                skinGos[i].ChildAct(IsSkin(l, i) && i == idx, 3);
                skinGos[i].ChildAct(IsSkin(l, i, 2), 4);
            }
            SkinCrt(idx);
        }
    }
    void SkinSelBtn(Col col, string txt) {
        skinSelectGo.Img().color = col.bg;
        skinSelectGo.Shadow().effectColor = col.shadow;
        skinSelectGo.Child(0).Txt().text = txt;
    }
    void SkinCrt(int idx) {
        skinModelGo.Child(0).RenMat().mainTexture = skinSprs[idx].texture;
    }
    class Btn {
        public Image bgImg, priceBgImg, priceImg;
        public Shadow bgShadow;
        public Text lvlTxt, priceTxt;
        public int maxLvl;
        public Btn(BtnData data) {
            bgImg = data.btn.Child(0).Img();
            bgShadow = bgImg.Shadow();
            lvlTxt = bgImg.Child(2).Txt();
            priceBgImg = data.btn.Child(1).Img();
            priceTxt = priceBgImg.Child(0).Txt();
            priceImg = priceBgImg.Child(1).Img();
            maxLvl = data.maxLvl;
        }
        public void Upd(Col col, bool isMax, int lvl, int price) {
            bgImg.color = col.bg;
            priceBgImg.color = col.bor;
            bgShadow.effectColor = col.shadow;
            lvlTxt.text = isMax ? "MAX LVL" : "LVL " + lvl;
            priceTxt.text = "" + price;
            priceTxt.Act(!isMax);
            priceImg.Act(!isMax);
        }
    }
    class Tab {
        public Transform contentTf;
        public RectTransform rt;
        public Image borImg, bgImg;
        public Tab(Transform contentTf, Transform tabTf) {
            this.contentTf = contentTf;
            rt = tabTf.Rt();
            borImg = tabTf.Img();
            bgImg = tabTf.Child(0).Img();
        }
        public void Upd(Vector2 p, Vector2 sz, Col col) {
            rt.anchoredPosition = p;
            rt.sizeDelta = sz;
            borImg.color = col.bor;
            bgImg.color = col.bg;
        }
    }
    class Lb {
        public Image bgImg;
        public Text txt;
        public Shadow shadow;
        public Lb(GameObject go, int i) {
            bgImg = go.Child(i).Img();
            txt = go.Child(i, 0).Txt();
            shadow = txt.Shadow();
        }
    }
    class Lb2 {
        public GameObject deadGo;
        public Image bgImg;
        public Text placeTxt, nameTxt, killTxt, scrTxt;
        public Shadow shadow;
        public Lb2(GameObject go, int i) {
            deadGo = go.ChildGo(i, 1);
            placeTxt = go.Child(i, 0).Txt();
            bgImg = go.Child(i).Img();
            nameTxt = go.Child(i, 2, 0).Txt();
            shadow = nameTxt.Shadow();
            killTxt = go.Child(i, 2, 1, 0).Txt();
            scrTxt = go.Child(i, 2, 2, 0).Txt();
        }
    }
    [Serializable]
    public class PlayerPrefsData {
        public Data key;
        public GameObject go;
    }
    [Serializable]
    public class BtnData {
        public Button btn;
        public int maxLvl;
    }
    [Serializable]
    public class SkinData {
        public string name, info;
        public Sprite spr;
        public int price;
    }
    [Serializable]
    public class Skin2Data {
        public List<Sprite> sprs;
        public int price, dPrice, maxPrice;
        public int Price => M.Min(price + Gc.SkinCnt * dPrice, maxPrice);
    }
    [Serializable]
    public class Col { public Color bg, bor, shadow; }
}