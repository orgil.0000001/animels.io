using UnityEngine;
using System.Collections.Generic;

public class HWP : Singleton<HWP> {
    public Vector3 offset;
    public Font font;
    public Texture arrowIcon;
    public bool isTextRotate = true;
    public bool isDistance = false;
    List<Character> characters = null;
    static Camera cam => Camera.main;
    static Transform playerCam => cam.transform;
    int arrowSize;
    GUIStyle style;
    [HideInInspector] public GUIStyle textStyle;
    public void Init(List<Character> chars) {
        characters = chars;
        arrowSize = (Screen.width + Screen.height) / 70;
        style = new GUIStyle(textStyle);
        style.normal.textColor = Color.white;
        style.font = font;
        style.fontSize = (Screen.width + Screen.height) / 70;
        style.fontStyle = FontStyle.Normal;
        style.alignment = TextAnchor.MiddleCenter;
        style.wordWrap = false;
        style.richText = true;
        style.clipping = TextClipping.Overflow;
        style.imagePosition = ImagePosition.TextOnly;
        style.contentOffset = Vector2.zero;
        style.fixedWidth = 0;
        style.fixedHeight = 0;
        style.stretchWidth = true;
        style.stretchHeight = false;
    }
    void OnGUI() {
        if (characters == null) return;
        foreach (var hud in characters) {
            Vector3 pos = hud.transform.position;
            var lb = hud.lb;
            GUI.color = lb.col;
            if (IsGame && lb.isLive) {
                Vector3 pos2 = pos + offset;
                if (Vector3.Dot(playerCam.forward, pos2 - playerCam.position) > 0f) {
                    string text = lb.name + "\n" + lb.scr;
                    if (font != null && !string.IsNullOrEmpty(text)) {
                        Vector2 size = style.CalcSize(new GUIContent(text));
                        Vector2 p = cam.WorldToViewportPoint(pos2);
                        p = new Vector2(p.x * Screen.width, (1 - p.y) * Screen.height);
                        GUI.Label(new Rect(p.x - size.x / 2, p.y - size.y, size.x, size.y), text, style);
                    }
                }
                if (arrowIcon != null) {
                    Vector2 arrowPos = ArrowPos(pos);
                    if (!IsOnScreen(pos)) {
                        float rot = Rot(arrowPos);
                        Vector2 rotVec = new Vector2(Mathf.Cos(rot * Mathf.Deg2Rad), Mathf.Sin(rot * Mathf.Deg2Rad));
                        Vector2 pivot = GetPivot(arrowPos.x, arrowPos.y, 0);
                        Matrix4x4 matrix = GUI.matrix;
                        Vector2 arrowPivot = pivot - rotVec * arrowSize * .6f;
                        GUIUtility.RotateAroundPivot(rot - 90, arrowPivot);
                        GUI.DrawTexture(new Rect(arrowPivot.x - arrowSize / 2, arrowPivot.y - arrowSize / 2, arrowSize, arrowSize), arrowIcon);
                        GUI.matrix = matrix;
                        string text = "" + lb.scr;
                        text = isDistance ? (string.IsNullOrEmpty(text) ? "" : text + "\n") + coloredStr("[" + disStr(pos) + "]") : text;
                        if (font != null && !string.IsNullOrEmpty(text)) {
                            Vector2 size = style.CalcSize(new GUIContent(text));
                            float dis = arrowSize * 1.2f;
                            if (isTextRotate) {
                                Vector2 p = pivot - rotVec * (dis + size.y / 2);
                                GUIUtility.RotateAroundPivot(rot - 90, p);
                                GUI.Label(new Rect(p.x - size.x / 2, p.y - size.y / 2, size.x, size.y), text, style);
                                GUI.matrix = matrix;
                            } else {
                                Vector2 p = (int)pivot.x == 0 || (int)pivot.x == Screen.width ?
                                    new Vector2((int)pivot.x == 0 ? dis : Screen.width - dis - size.x, Mathf.Clamp(pivot.y - size.y / 2 - rotVec.y * dis, 0, Screen.height - size.y)) :
                                    new Vector2(Mathf.Clamp(pivot.x - size.x / 2 - rotVec.x * dis, 0, Screen.width - size.x), (int)pivot.y == 0 ? dis : Screen.height - dis - size.y);
                                GUI.Label(new Rect(p.x, p.y, size.x, size.y), text, style);
                            }
                        }
                    }
                }
            }
        }
    }
    string disStr(Vector3 pos) { return string.Format("{0:N0}m", Vector3.Distance(P.Tp, pos)); }
    string coloredStr(string text, string hex = "ffffff") { return "<color=#" + hex + ">" + text + "</color>"; }
    float Rot(Vector2 b) {
        Vector2 a = new Vector2(cam.pixelWidth / 2, cam.pixelHeight / 2);
        return Mathf.Atan((b.y - a.y) / (b.x - a.x)) * 180 / Mathf.PI + (b.x < a.x ? 180 : 0);
    }
    Vector2 ArrowPos(Vector3 pos) {
        Vector3 dir = cam.transform.InverseTransformDirection(pos - cam.transform.position).normalized / 5;
        return new Vector2(cam.pixelWidth * (0.5f + dir.x * 20f / cam.aspect), cam.pixelHeight * (0.5f - dir.y * 20f));
    }
    static bool IsOnScreen(Vector3 p) {
        Vector3 pos = cam.WorldToScreenPoint(p);
        pos = new Vector3(pos.x / cam.pixelWidth, pos.y / cam.pixelHeight, p.z);
        if (Vector3.Dot(p - cam.transform.position, cam.transform.forward) <= 0) return false;
        float margen = 0.001f;
        return !(pos.x < margen || pos.x > 1 - margen || pos.y < margen || pos.y > 1 - margen);
    }
    public static bool IsOnScreenBox(Vector3 p, float x, float z) {
        return IsOnScreen(p + new Vector3(-x, 0, -z)) || IsOnScreen(p + new Vector3(x, 0, -z)) ||
            IsOnScreen(p + new Vector3(-x, 0, z)) || IsOnScreen(p + new Vector3(x, 0, z));
    }
    Vector2 GetPivot(float h, float v, float size) {
        Vector2 pivot = Vector2.zero;
        Vector2 hv = new Vector2(h - cam.pixelWidth / 2f, v - cam.pixelHeight / 2f);
        Vector2 slope = new Vector2(hv.y / hv.x, (float)cam.pixelHeight / (float)cam.pixelWidth);
        Vector2 mid = new Vector2(Camera.main.pixelWidth / 2f, Camera.main.pixelHeight / 2f);
        float num;
        if (slope.x > slope.y || slope.x < -slope.y) {
            num = (mid.y - size / 2) / hv.y;
            if (hv.y < 0) {
                pivot.y = size / 2;
                num = -num;
            } else {
                pivot.y = cam.pixelHeight - size / 2;
            }
            pivot.x = mid.x + hv.x * num;
            return pivot;
        }
        num = (mid.x - size / 2) / hv.x;
        if (hv.x < 0) {
            pivot.x = size / 2;
            num = -num;
        } else {
            pivot.x = cam.pixelWidth - size / 2;
        }
        pivot.y = mid.y + hv.y * num;
        return pivot;
    }
}